/* $Id$ */
/*
 * Copyright (C) 2015 Teluu Inc. (http://www.teluu.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.pjsip;

import android.view.SurfaceView;

import org.apache.sip.backend.sip.PjCoreHolder;

public class PjCamera {
//    private final String TAG = "PjCamera";

    public PjCamera(int idx, int w, int h, int fmt, int fps, long userData_, SurfaceView surface) {
//        Log.d(TAG, Thread.currentThread() + " хочет создать камеру №" + idx);
        PjCoreHolder.INSTANCE.getPjCore().getCameraThread().setNewCamera(idx, w, h, fmt, fps, userData_);
    }

    private int switchResult = 100;
    public int SwitchDevice(int idx) {
        Object lock = new Object();
//        Log.d(TAG, Thread.currentThread() + " хочет сменить камеру на №" + idx);

        PjCoreHolder.INSTANCE.getPjCore().getCameraThread().switchCamera(idx, lock, new CameraThread.OperationResultListener() {
            @Override
            public void onResult(int res) {
                switchResult  = res;
            }
        });

        synchronized (lock) {
            while (switchResult == 100) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    //А пох
                }
            }
        }

        int result = switchResult;
        switchResult = 100;
        return result == 100 ? -10 : result;
    }

    private int startResult = 100;
    public int Start() {
        Object lock = new Object();
//        Log.d(TAG, Thread.currentThread() + " хочет стартануть камеру");

        PjCoreHolder.INSTANCE.getPjCore().getCameraThread().startCamera(lock, new CameraThread.OperationResultListener() {
            @Override
            public void onResult(int res) {
                startResult  = res;
            }
        });

        synchronized (lock) {
            while (startResult == 100) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    //А пох
                }
            }
        }

        int result = startResult;
        startResult = 100;
        return result == 100 ? -10 : result;
    }


    public void Stop() {
//        Log.d(TAG, Thread.currentThread() + " хочет остановить камеру");
        PjCoreHolder.INSTANCE.getPjCore().getCameraThread().stopCamera();
    }

    public static native void PushFrame(byte[] data, int length, long userData_);

}
