package org.pjsip;

import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Handler;
import android.os.HandlerThread;

import org.apache.ThreadDirector;

import java.io.IOException;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 03 Сентябрь 2017.
 */

public class CameraThread extends HandlerThread implements Camera.PreviewCallback, Camera.ErrorCallback {

//    private final String TAG = "CameraThread";

    private Handler mHandler;

    public CameraThread(String name) {
        super(name);
    }

    @Override
    public synchronized void start() {
        super.start();
        mHandler = new Handler(getLooper());
    }

    public void post(Runnable task) {
        mHandler.post(task);
    }

    public void postDelayed(Runnable task, long delay) {
        mHandler.postDelayed(task, delay);
    }

    public void removeCallbacks(Runnable task) {
        mHandler.removeCallbacks(task);
    }

    public void prepareHandler() {
        mHandler = new Handler(getLooper());
    }

    private int width;
    private int height;
    private int format;
    private int fps1000;

    private Camera camera = null;

    private boolean isRunning = false;
    private int camIdx;
    private long userData;
    private byte[] buffer;

    private SurfaceTexture defaultSurfaceTexture = null;

    private SurfaceTexture surfaceTexture = null;

    synchronized void setNewCamera(final int idx, final int w, final int h, final int fmt, final int fps, final long userData_) {

        post(new Runnable() {
            @Override
            public void run() {

                if (camera != null) stopCameraNow();

                width = w;
                height = h;
                format = fmt;
                fps1000 = fps;

                camIdx = idx;
                userData = userData_;

                if (defaultSurfaceTexture == null) defaultSurfaceTexture = new SurfaceTexture(10);
            }
        });
    }

    private Runnable changeSurfaceRunnable;

    public synchronized void setSurfaceTexture(final SurfaceTexture newSurfaceTexture) {
//        Log.d(TAG, Thread.currentThread() + " Назначаю тестурный сурфейс " + newSurfaceTexture + ". №" + camIdx);
        if (changeSurfaceRunnable != null) {
//            Log.d(TAG, Thread.currentThread() + " Удаляю запланированные назначения. №" + camIdx);
            removeCallbacks(changeSurfaceRunnable);
        }
        if (newSurfaceTexture != null) {
//            Log.d(TAG, Thread.currentThread() + " Планирую удаляемое назначение сурфейса " + newSurfaceTexture + ". №" + camIdx);
            changeSurfaceRunnable = new Runnable() {
                @Override
                public void run() {
                    changeSurfaceRunnable = null;
                    setSurfaceTextureNow(newSurfaceTexture);
                }
            };
            post(changeSurfaceRunnable);
        } else {
//            Log.d(TAG, Thread.currentThread() + " Планирую неудаляемое назначение сурфейса " + null + ". №" + camIdx);
            post(new Runnable() {
                @Override
                public void run() {
                    setSurfaceTextureNow(null);
                }
            });
        }
    }

    private void setSurfaceTextureNow(SurfaceTexture newSurfaceTexture) {
//        Log.d(TAG, Thread.currentThread() + " Назначаю тестурный сурфейс " + newSurfaceTexture + ". №" + camIdx);
        if (newSurfaceTexture == null) {
            if (camera != null) {
                try {
//                    Log.d(TAG, Thread.currentThread() + " Останавливаю предпросмотр. №" + camIdx);
                    camera.stopPreview();
//                    Log.d(TAG, Thread.currentThread() + " Даем дефолтный сурфейс. №" + camIdx);
                    camera.setPreviewTexture(defaultSurfaceTexture);
//                    Log.d(TAG, Thread.currentThread() + " Добавляю буфер. №" + camIdx);
                    camera.addCallbackBuffer(buffer);
//                    Log.d(TAG, Thread.currentThread() + " Назначаю себя в качестве колбэка ошибок. №" + camIdx);
                    camera.setErrorCallback(this);
//                    Log.d(TAG, Thread.currentThread() + " Назначаю себя в качестве колбэка превью. №" + camIdx);
                    camera.setPreviewCallbackWithBuffer(this);
//                    Log.d(TAG, Thread.currentThread() + " Начинаю предпросмотр. №" + camIdx);
                    camera.startPreview();
                } catch (IOException | RuntimeException e) {
//                    Log.d(TAG, Thread.currentThread() + " Не удалось." + camIdx);
                    e.printStackTrace();
                }
            }
        } else {
            if (camera != null) {
                try {
//                    Log.d(TAG, Thread.currentThread() + " Останавливаю предпросмотр. №" + camIdx);
                    camera.stopPreview();
//                    Log.d(TAG, Thread.currentThread() + " Даем наш сурфейс " + newSurfaceTexture + ". №" + camIdx);
                    camera.setPreviewTexture(newSurfaceTexture);
//                    Log.d(TAG, Thread.currentThread() + " Добавляю буфер. №" + camIdx);
                    camera.addCallbackBuffer(buffer);
//                    Log.d(TAG, Thread.currentThread() + " Назначаю себя в качестве колбэка ошибок. №" + camIdx);
                    camera.setErrorCallback(this);
//                    Log.d(TAG, Thread.currentThread() + " Назначаю себя в качестве колбэка превью. №" + camIdx);
                    camera.setPreviewCallbackWithBuffer(this);
//                    Log.d(TAG, Thread.currentThread() + " Начинаю предпросмотр. №" + camIdx);
                    camera.startPreview();
                } catch (IOException | RuntimeException e) {
//                    Log.d(TAG, Thread.currentThread() + " Не удалось." + camIdx);
                    e.printStackTrace();
                }
            }
        }
        surfaceTexture = newSurfaceTexture;
    }

    synchronized void stopCamera() {
        post(new Runnable() {
            @Override
            public void run() {
                stopCameraNow();
            }
        });
    }

    private void stopCameraNow() {
        isRunning = false;
        if (camera == null)
            return;

        try {
            camera.setErrorCallback(null);
//        Log.d(TAG, Thread.currentThread() + " Назначаю null в качестве колбэка. №" + camIdx);
            camera.setPreviewCallbackWithBuffer(null);
//        Log.d(TAG, Thread.currentThread() + " Останавливаю предпросмотр. №" + camIdx);
            camera.stopPreview();
//        Log.d(TAG, Thread.currentThread() + " отпускаю камеру. №" + camIdx);
            camera.release();
        } catch (Exception e) {
            camera.release();
        } finally {
            buffer = null;
            camera = null;
        }
    }

    synchronized void startCamera(final Object lock, final OperationResultListener listener) {
        post(new Runnable() {
            @Override
            public void run() {
                int res = startCameraNow();
                if (lock != null) {
                    synchronized (lock) {
                        if (listener != null) listener.onResult(res);
                        lock.notifyAll();
                    }
                }
            }
        });
    }

    private int startCameraNow() {
//        Log.d(TAG, Thread.currentThread() + " Пытаюсь стартануть камеру. №" + camIdx);
//        if (PjCoreHolder.INSTANCE.getPjCore().getPjCamera() != this) {
//            Log.d(TAG, Thread.currentThread() + " Другая камера уже работает, сушу весла. №" + camIdx);
//            return 0;
//        }

        try {
//            Log.d(TAG, Thread.currentThread() + " Открываю камеру. №" + camIdx);
            camera = Camera.open(camIdx);
        } catch (Exception e) {
            e.printStackTrace();
//            Log.d(TAG, Thread.currentThread() + " Не удалось. №" + camIdx);
            return -10;
        }

        try {
            if (surfaceTexture != null && changeSurfaceRunnable == null) {
//                Log.d(TAG, Thread.currentThread() + " Даем наш сурфейс " + surfaceTexture + ". №" + camIdx);
                camera.setPreviewTexture(surfaceTexture);
            } else {
//                Log.d(TAG, Thread.currentThread() + " Даем дефолтный сурфейс. №" + camIdx);
                camera.setPreviewTexture(defaultSurfaceTexture);
            }
        } catch (Exception e) {
            e.printStackTrace();
            camera.release();
            return -20;
        }

        try {
            Camera.Parameters cp = camera.getParameters();
            cp.setPreviewSize(width, height);
            cp.setPreviewFormat(format);
            // Some devices such as Nexus require an exact FPS range from the
            // supported FPS ranges, specifying a subset range will raise
            // exception.
            //cp.setPreviewFpsRange(param.fps1000, param.fps1000);
//            Log.d(TAG, Thread.currentThread() + " Назначаю параметры камере. №" + camIdx);
            camera.setParameters(cp);
        } catch (Exception e) {
            e.printStackTrace();
//            Log.d(TAG, Thread.currentThread() + " Не удалось. №" + camIdx);
            camera.release();
            return -30;
        }

        try {
            buffer = new byte[width * height * ImageFormat.getBitsPerPixel(format) / 8];
//            Log.d(TAG, Thread.currentThread() + " Добавляю буфер. №" + camIdx);
            camera.addCallbackBuffer(buffer);
//            Log.d(TAG, Thread.currentThread() + " Назначаю себя в качестве колбэка ошибок. №" + camIdx);
            camera.setErrorCallback(this);
//            Log.d(TAG, Thread.currentThread() + " Назначаю себя в качестве колбэка превью. №" + camIdx);
            camera.setPreviewCallbackWithBuffer(this);
//            Log.d(TAG, Thread.currentThread() + " Начинаю предпросмотр. №" + camIdx);
            camera.startPreview();
            isRunning = true;
        } catch (Exception e) {
            isRunning = true;
            return -40;
        }
        return 0;
    }

    public void switchCamera(final int idx, final Object lock, final OperationResultListener listener) {
        post(new Runnable() {
            @Override
            public void run() {
                final int res = switchCameraNow(idx);
                if (lock != null) {
                    synchronized (lock) {
                        if (listener != null) listener.onResult(res);
                        lock.notifyAll();
                    }
                } else if (listener != null) {
                    ThreadDirector.INSTANCE.postToWorker(new Runnable() {
                        @Override
                        public void run() {
                            listener.onResult(res);
                        }
                    });
                }
            }
        });
    }

    private int switchCameraNow(int idx) {
//        Log.d(TAG, Thread.currentThread() + " Меняю камеру с №" + camIdx + " на №" + idx);
        boolean isCaptureRunning = isRunning;
        int oldIdx = camIdx;

        if (oldIdx == idx) return 0;

        if (isCaptureRunning)
            stopCameraNow();

        camIdx = idx;

        if (isCaptureRunning) {
            int ret = startCameraNow();
            if (ret != 0) {
        /* Try to revert back */
                stopCameraNow();
                camIdx = oldIdx;
                startCameraNow();
                return ret;
            }
        }

        return 0;
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        //TODO: костыль. Хуй его знает, что у дрочеров тут может быть нулем !!1!!1!!1!
        if (data == null || camera == null) return;
//        Log.d(TAG, Thread.currentThread() + " я получил фрейм превью. №" + camIdx);
        if (isRunning) {
//            Log.d(TAG, Thread.currentThread() + " отправляю фрейм превью в либу. №" + camIdx);
            PjCamera.PushFrame(data, data.length, userData);
//            Log.d(TAG, Thread.currentThread() + " Добавляю буфер. №" + camIdx);
            camera.addCallbackBuffer(buffer);
        }
    }

    @Override
    public void onError(int i, Camera camera) {
        switch (i) {
            case Camera.CAMERA_ERROR_EVICTED:
                if (isRunning) {
                    stopCameraNow();
                }
                break;
            case Camera.CAMERA_ERROR_SERVER_DIED:
                if (isRunning) {
                    stopCameraNow();
                    startCameraNow();
                }
                break;
            case Camera.CAMERA_ERROR_UNKNOWN:
                if (isRunning) {
                    stopCameraNow();
                    startCameraNow();
                }
                break;
        }
    }

    // Что-то это бестолку
//    public void setDisplayRotation(int rotation) {
//        if (isRunning && camera != null) {
//            Camera.CameraInfo info = new Camera.CameraInfo();
//            Camera.getCameraInfo(camIdx, info);
//
//            int result;
//            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//                result = (info.orientation + rotation) % 360;
//                result = (360 - result) % 360;  // compensate the mirror
//            } else {  // back-facing
//                result = (info.orientation - rotation + 360) % 360;
//            }
//            camera.setDisplayOrientation(result);
//            camera.getParameters().setRotation(result);
//        }
//    }

    public interface OperationResultListener {
        void onResult(int res);
    }
}
