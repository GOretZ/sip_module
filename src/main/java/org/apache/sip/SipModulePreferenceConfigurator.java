package org.apache.sip;

import android.content.Context;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;

import org.apache.settings.ModulePreference;
import org.apache.settings.PreferenceStorage;
import org.apache.sip.entity.state.RuntimeCache;
import org.apache.sip.texts.TextField;
import org.apache.tools.StorageFileTools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 05 Август 2017.
 */

public class SipModulePreferenceConfigurator {

    public static final String CAMERA_CATEGORY_KEY = "camera";
    public static final String CAMERA_ROTATE_FRONT_CAMERA = "camera_rotate_front";
    public static final String CAMERA_ROTATE_BACK_CAMERA = "camera_rotate_back";
    public static final String SHARED_CAMERA_SETTINGS = "camera_flags";

    public static void init(Context context) {
        RuntimeCache.INSTANCE.setRotateFront180(PreferenceManager.getDefaultSharedPreferences(context).getBoolean(CAMERA_ROTATE_FRONT_CAMERA, false));
        RuntimeCache.INSTANCE.setRotateBack180(PreferenceManager.getDefaultSharedPreferences(context).getBoolean(CAMERA_ROTATE_BACK_CAMERA, false));
        PreferenceStorage.INSTANCE.addModulePreference(new ModulePreference() {
            @Override
            public PreferenceCategory getPreferenceCategory(Context context) {
                PreferenceCategory category = new PreferenceCategory(context);
                category.setTitle(TextField.VIDEO_CAMERA.getValue());
                category.setKey(CAMERA_CATEGORY_KEY);
                return category;
            }

            @Override
            public List<Preference> getPreferences(Context context) {
                ArrayList<Preference> preferences = new ArrayList<>();
                SwitchPreference switchPreference = new SwitchPreference(context);
                switchPreference.setTitle(TextField.ROTATE_FRONT_CAM.getValue());
                switchPreference.setKey(CAMERA_ROTATE_FRONT_CAMERA);
                switchPreference.setDefaultValue(Boolean.FALSE);
                switchPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        Boolean flag = (Boolean) newValue;
                        RuntimeCache.INSTANCE.setRotateFront180(flag);
                        String saveString = (flag ? "1" : "0") + (RuntimeCache.INSTANCE.isRotateBack180() ? "1" : "0");
                        try {
                            StorageFileTools.saveEncryptedStringToFile(SHARED_CAMERA_SETTINGS, saveString, SipConfigPortal.INSTANCE.getSalt());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return true;
                    }
                });
                preferences.add(switchPreference);
                switchPreference = new SwitchPreference(context);
                switchPreference.setTitle(TextField.ROTATE_BACK_CAM.getValue());
                switchPreference.setKey(CAMERA_ROTATE_BACK_CAMERA);
                switchPreference.setDefaultValue(Boolean.FALSE);
                switchPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        Boolean flag = (Boolean) newValue;
                        RuntimeCache.INSTANCE.setRotateBack180(flag);
                        String saveString = (RuntimeCache.INSTANCE.isRotateFront180() ? "1" : "0") + (flag ? "1" : "0");
                        try {
                            StorageFileTools.saveEncryptedStringToFile(SHARED_CAMERA_SETTINGS, saveString, SipConfigPortal.INSTANCE.getSalt());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return true;
                    }
                });
                preferences.add(switchPreference);
                return preferences;
            }

            @Override
            public String getKeyDependency(int position) {
                return null;
            }

        });
    }
}
