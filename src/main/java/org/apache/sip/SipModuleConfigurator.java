package org.apache.sip;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Size;

import org.apache.ThreadDirector;
import org.apache.api.ApiUserModuleContract;
import org.apache.registration.GetTextsListener;
import org.apache.registration.GetTextsListenersCenter;
import org.apache.registration.entity.ErrorType;
import org.apache.registration.entity.ExternalTask;
import org.apache.registration.entity.state.RuntimeCache;
import org.apache.sip.backend.sip.PjAccount;
import org.apache.sip.backend.sip.PjCoreHolder;
import org.apache.sip.backend.www.ApiCalls;
import org.apache.sip.entity.SipSettings;
import org.apache.sip.texts.TextField;
import org.apache.tools.StorageFileTools;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.apache.sip.SipModulePreferenceConfigurator.CAMERA_ROTATE_BACK_CAMERA;
import static org.apache.sip.SipModulePreferenceConfigurator.CAMERA_ROTATE_FRONT_CAMERA;
import static org.apache.sip.SipModulePreferenceConfigurator.SHARED_CAMERA_SETTINGS;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 17 янавря 2017.
 * Конфигуратор модуля SIP
 */

public class SipModuleConfigurator implements ApiUserModuleContract{

    //Инициализация конфигуратора
    public static SipModuleConfigurator init(Context context){
        RuntimeCache.INSTANCE.addExternalTask(new ExternalTask() {
            @Override
            public void run(@NonNull final TaskResult taskResult) {
            ApiCalls.INSTANCE.getApiMethods().getSipSettings(RuntimeCache.INSTANCE.getCurrentUser().getGender()).enqueue(new Callback<SipSettings>() {
                @Override
                public void onResponse(Call<SipSettings> call, Response<SipSettings> response) {
                    SipSettings sipSettings = response.body();
                    if (sipSettings == null) {
                        taskResult.onFail(ErrorType.NO_CONNECTION);
                    } else {
                        org.apache.sip.entity.state.RuntimeCache.INSTANCE.setSipSettings(sipSettings);
                        if (!PjCoreHolder.INSTANCE.getPjCore().init()) {
                            taskResult.onFail(ErrorType.NO_CONNECTION);
                            return;
                        }
                        String userId = "u" + RuntimeCache.INSTANCE.getCurrentUser().getUserId();
                        //host = "51.255.68.51";
                        String host = sipSettings.getSipHost();
                        //port = "5067";
                        int port = sipSettings.getSipPort();
                        //mTransport = transport = "transport=tcp";
                        String transport = sipSettings.getSipTransport();
                        //password = "321456";
                        String password = sipSettings.getSipPassword();
                        try {
                            PjCoreHolder.INSTANCE.getPjCore().authorize(userId, password, host, port, transport, new PjAccount.OnAuthListener() {
                                @Override
                                public void onSuccess() {
                                    taskResult.onSuccess();
                                }

                                @Override
                                public void onServerFailed(String reason) {
                                    taskResult.onSuccess();
                                }

                                @Override
                                public void onClientFailed(String reason) {
                                    taskResult.onSuccess();
                                }
                            });
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                            taskResult.onFail(ErrorType.NO_CONNECTION);
                        }
                    }
                }

                @Override
                public void onFailure(Call<SipSettings> call, Throwable t) {
                    taskResult.onFail(ErrorType.NO_CONNECTION);
                }
            });
            }
        });

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        RuntimeCache.INSTANCE.addExternalTask(new ExternalTask() {
            @Override
            public void run(@NonNull TaskResult taskResult) {
                try {
                    String flagsString = StorageFileTools.getEncryptedStringFromFile(SHARED_CAMERA_SETTINGS, SipConfigPortal.INSTANCE.getSalt());
                    if (flagsString != null) {
                        if (flagsString.length() > 0) {
                            if (flagsString.charAt(0) == '1') {
                                org.apache.sip.entity.state.RuntimeCache.INSTANCE.setRotateFront180(true);
                                sharedPreferences.edit().putBoolean(CAMERA_ROTATE_FRONT_CAMERA, true).apply();
                            } else if (flagsString.charAt(0) == '0') {
                                org.apache.sip.entity.state.RuntimeCache.INSTANCE.setRotateFront180(false);
                                sharedPreferences.edit().putBoolean(CAMERA_ROTATE_FRONT_CAMERA, false).apply();
                            }
                        }
                        if (flagsString.length() > 1) {
                            if (flagsString.charAt(1) == '1') {
                                org.apache.sip.entity.state.RuntimeCache.INSTANCE.setRotateBack180(true);
                                sharedPreferences.edit().putBoolean(CAMERA_ROTATE_BACK_CAMERA, true).apply();
                            } else if (flagsString.charAt(1) == '0') {
                                org.apache.sip.entity.state.RuntimeCache.INSTANCE.setRotateBack180(false);
                                sharedPreferences.edit().putBoolean(CAMERA_ROTATE_BACK_CAMERA, false).apply();
                            }
                        }
                    }
                    taskResult.onSuccess();
                } catch (IOException e) {
                    e.printStackTrace();
                    taskResult.onFail(ErrorType.STORAGE_ERROR);
                }
            }
        });

        GetTextsListenersCenter.INSTANCE.addGetTextListener(
                new GetTextsListener() {
                    @Override
                    protected Class getTextsEnum() {
                        return TextField.class;
                    }
                }
        );

        return new SipModuleConfigurator();
    }

    @Override
    public void initializeApi(final String baseUrl, final String siteKey, @Size(2) final String language){ //Метод для инициализации апи
        ThreadDirector.INSTANCE.postToWorker(new Runnable() {
            @Override
            public void run() {
                ApiCalls.INSTANCE.initializeApi(baseUrl, siteKey, language);
            }
        });
    }
}