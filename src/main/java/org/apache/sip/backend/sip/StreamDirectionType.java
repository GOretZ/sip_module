package org.apache.sip.backend.sip;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 25 Октябрь 2016.
 */

// Направление потока
@IntDef({
        StreamDirectionType.STREAM_ONLY,
        StreamDirectionType.BOTH,
        StreamDirectionType.WATCH_ONLY
})
@Retention(RetentionPolicy.SOURCE)
public @interface StreamDirectionType {
    int STREAM_ONLY = -1;
    int BOTH = 0;
    int WATCH_ONLY = 1;

}
