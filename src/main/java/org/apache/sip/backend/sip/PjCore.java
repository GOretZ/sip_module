package org.apache.sip.backend.sip;

import android.hardware.Camera;
import android.os.Process;
import android.support.annotation.NonNull;

import org.apache.sip.entity.CameraDevice;
import org.pjsip.CameraThread;
import org.pjsip.pjsua2.AccountConfig;
import org.pjsip.pjsua2.AuthCredInfo;
import org.pjsip.pjsua2.AuthCredInfoVector;
import org.pjsip.pjsua2.CallOpParam;
import org.pjsip.pjsua2.Endpoint;
import org.pjsip.pjsua2.EpConfig;
import org.pjsip.pjsua2.LogConfig;
import org.pjsip.pjsua2.StringVector;
import org.pjsip.pjsua2.TransportConfig;
import org.pjsip.pjsua2.UaConfig;
import org.pjsip.pjsua2.VidCodecParam;
import org.pjsip.pjsua2.pj_log_decoration;
import org.pjsip.pjsua2.pjmedia_aud_dev_route;
import org.pjsip.pjsua2.pjsip_transport_type_e;

import java.util.ArrayList;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 11 Февраль 2017.
 */

public class PjCore {

    private Endpoint mEndpoint;

    private AccountConfig mAccountConfig;
    private PjAccount mAccount;

    private EpConfig mEndpointConfig;

    private TransportConfig mSipTransportConfig;
    private CameraThread cameraThread;

    //private boolean readyForJob = false;
    //private CameraDevice mCameraDevice = new CameraDevice();

    /* Maintain reference to log writer to avoid premature cleanup by GC */
    private PjLogWriter pjLogWriter;

    //private final int SIP_PORT  = 6101;
    private final int LOG_LEVEL = 1;

    public CameraThread getCameraThread() {
        return cameraThread;
    }

    public boolean init() {
        boolean res = init(false);
        cameraThread = new CameraThread("Camera thread");
        cameraThread.start();
        cameraThread.prepareHandler();
        return res;
    }

    public boolean init(boolean ownWorkerThread) {
        try {
            System.loadLibrary("opus");
            System.loadLibrary("openh264");
            System.loadLibrary("pjsua2");
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
            return false;
        }

        /* Create endpoint */
        try {
            mEndpoint = new Endpoint();
            mEndpoint.libCreate();
        } catch (Exception e) {
            e.printStackTrace();
            new IllegalStateException("Либа не создалась").printStackTrace();
			Process.killProcess(Process.myPid());
        }

        /* Create endpoint config */
        mEndpointConfig = new EpConfig();

        /* Override log level setting */
        mEndpointConfig.getLogConfig().setLevel(LOG_LEVEL);
        mEndpointConfig.getLogConfig().setConsoleLevel(LOG_LEVEL);

        /* Set log config. */
		if (pjLogWriter == null) pjLogWriter = new PjLogWriter();
		LogConfig logConfig = mEndpointConfig.getLogConfig();
        logConfig.setWriter(pjLogWriter);
        logConfig.setDecor(logConfig.getDecor() & ~(pj_log_decoration.PJ_LOG_HAS_CR.swigValue() | pj_log_decoration.PJ_LOG_HAS_NEWLINE.swigValue()));

        /* Set ua config. */
        UaConfig uaConfig = mEndpointConfig.getUaConfig();
        uaConfig.setUserAgent("Pjsua2 Android " + mEndpoint.libVersion().getFull());
        uaConfig.setNatTypeInSdp(0);
        //StringVector stunServers = new StringVector();
        //stunServers.add("stun.l.google.com:19302");
        //uaConfig.setStunServer(stunServers);
        if (ownWorkerThread) {
            uaConfig.setThreadCnt(0);
            uaConfig.setMainThreadOnly(true);
        }

        /* Init endpoint */
        try {
            mEndpoint.libInit(mEndpointConfig);
        } catch (Exception e) {
            e.printStackTrace();
            new IllegalStateException("Либа не инициализировала Endpoint").printStackTrace();
			Process.killProcess(Process.myPid());
        }

        /* Create transports. */
        mSipTransportConfig = new TransportConfig();


        /* Set 'default' values */
        //mSipTransportConfig.setPort(SIP_PORT);
        //mSipTransportConfig.setPortRange(SIP_PORT);
        mSipTransportConfig.setPort(0);
        try {
            mEndpoint.transportCreate(pjsip_transport_type_e.PJSIP_TRANSPORT_UDP, mSipTransportConfig);
        } catch (Exception e) {
            e.printStackTrace();
            new IllegalStateException("UDP транспорт не создался").printStackTrace();
			Process.killProcess(Process.myPid());
		}

        try {
            mEndpoint.transportCreate(pjsip_transport_type_e.PJSIP_TRANSPORT_TCP, mSipTransportConfig);
        } catch (Exception e) {
            e.printStackTrace();
            new IllegalStateException("TCP транспорт не создался").printStackTrace();
			Process.killProcess(Process.myPid());
        }

        /* Start. */
        try {
            mEndpoint.libStart();
        } catch (Exception e) {
            e.printStackTrace();
            new IllegalStateException("Либа не стартовала").printStackTrace();
			Process.killProcess(Process.myPid());
        }

        try {
            VidCodecParam vidCodecParam = mEndpoint.getVideoCodecParam("H264");
            vidCodecParam.getEncFmt().setWidth(320);
            vidCodecParam.getEncFmt().setHeight(240);
            vidCodecParam.getEncFmt().setFpsNum(30);
            vidCodecParam.getEncFmt().setFpsDenum(1);
            vidCodecParam.getEncFmt().setMaxBps(512000);
            vidCodecParam.getEncFmt().setAvgBps(256000);
//            vidCodecParam.getDecFmt().setWidth(640);
//            vidCodecParam.getDecFmt().setHeight(480);
            vidCodecParam.getDecFmt().setFpsNum(30);
            vidCodecParam.getDecFmt().setFpsDenum(1);
            vidCodecParam.getDecFmtp().get(0).setName("profile-level-id");
            //vidCodecParam.getDecFmtp().get(0).setVal("420015");
            vidCodecParam.getDecFmtp().get(0).setVal("42E01F");
            mEndpoint.setVideoCodecParam("H264", vidCodecParam);
        } catch (Exception e) {
            e.printStackTrace();
            new IllegalStateException("Настройка видео обломалась").printStackTrace();
			Process.killProcess(Process.myPid());
        }

        try {
            /*CodecInfoVector codecInfoVector = mEndpoint.codecEnum();
            for (int i = 0; i < codecInfoVector.size(); i++){
                Log.d("Pjsip", codecInfoVector.get(i).getCodecId() + " (priority: " + codecInfoVector.get(i).getPriority() + ")");
            }*/
            mEndpoint.codecSetPriority("opus/48000/2", (short) 131);
            /*codecInfoVector = mEndpoint.codecEnum();
            for (int i = 0; i < codecInfoVector.size(); i++){
                Log.d("Pjsip", codecInfoVector.get(i).getCodecId() + " (priority: " + codecInfoVector.get(i).getPriority() + ")");
            }*/
            mEndpoint.audDevManager().setOutputRoute(pjmedia_aud_dev_route.PJMEDIA_AUD_DEV_ROUTE_LOUDSPEAKER, true);
        } catch (Exception e) {
            e.printStackTrace();
            new IllegalStateException("Настройка аудио обломалась").printStackTrace();
			Process.killProcess(Process.myPid());
		}
        return true;
    }

    public void refreshDevices() {
        try {
            mEndpoint.vidDevManager().refreshDevs();
            mEndpoint.audDevManager().refreshDevs();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<CameraDevice> cameraDevices = null;
    public ArrayList<CameraDevice> getCameraDevices(CameraDevice excludeDeivce){
        if (cameraDevices == null) {
            cameraDevices = new ArrayList<>();
            int count = Camera.getNumberOfCameras();
            for (int i = 0; i < count; i++) {
                Camera.CameraInfo ci = new Camera.CameraInfo();
                Camera.getCameraInfo(i, ci);

                CameraDevice cameraDevice = new CameraDevice();
                cameraDevice.setId(i);

                if (ci.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    cameraDevice.setFacing(CameraDevice.FACING_FRONT);
                } else if (ci.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    cameraDevice.setFacing(CameraDevice.FACING_BACK);
                }
                cameraDevice.setOrientation(ci.orientation);

    //            cameraDevice.setDriver(videoDevInfo.getDriver());
                cameraDevice.setName("Camera " + i);
    //            cameraDevice.setCapabilities(videoDevInfo.getCaps());
    //            cameraDevice.setDirection(videoDevInfo.getDir().toString());

                cameraDevices.add(cameraDevice);
            }
        }
        ArrayList<CameraDevice> cameraDevices1 = new ArrayList<>();
        for (CameraDevice cameraDevice : cameraDevices) {
            if (excludeDeivce != null && excludeDeivce.getId() == cameraDevice.getId()) continue;
            cameraDevices1.add(cameraDevice);
        }
        return cameraDevices1;
    }

    public void authorize(@NonNull String userId, @NonNull String password, @NonNull String host, int port, String transport,
                          PjAccount.OnAuthListener onAuthListener) {
        String idUri = "sip:"
                .concat(userId)
                .concat("@")
                .concat(host)
                .concat(":")
                .concat(String.valueOf(port))
                .concat((transport == null || transport.isEmpty() ? "" : ";transport=".concat(transport)));
        String registrarUri = "sip:"
                .concat(host)
                .concat(":")
                .concat(String.valueOf(port))
                .concat((transport == null || transport.isEmpty() ? "" : ";transport=".concat(transport)));
        /* Create account. */
        if (mAccountConfig == null) {
            mAccountConfig = new AccountConfig();
        } else if (mAccountConfig.getIdUri().equals(idUri)){
            onAuthListener.onSuccess();
            return;
        }
        mAccountConfig.setIdUri(idUri);
//        System.out.println("D/Pjsip: Register as " + mAccountConfig.getIdUri());
        mAccountConfig.getRegConfig().setRegistrarUri(registrarUri);
        AuthCredInfoVector streamCreds = mAccountConfig.getSipConfig().getAuthCreds();
        streamCreds.clear();
        streamCreds.add(new AuthCredInfo("Digest", "*", userId, 0, password));
        StringVector streamProxies = mAccountConfig.getSipConfig().getProxies();
        streamProxies.clear();
        //streamProxies.add("sip:" + mHost + ";" + mTransport);

        //mAccountConfig.getVideoConfig().setAutoTransmitOutgoing(true);
        mAccountConfig.getPresConfig().setPublishEnabled(true);
        try {
            if (mAccount == null) {
                mAccount = new PjAccount(mAccountConfig);
                mAccount.create(mAccountConfig, onAuthListener);
            } else {
                mAccount.modify(mAccountConfig, onAuthListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
            new IllegalArgumentException("Реквизиты для SIP регистрации кривые?").printStackTrace();
        }
    }

    public boolean setStreamVideoSize(int width, int height){
        try {
            VidCodecParam vidCodecParam = mEndpoint.getVideoCodecParam("H264");
            vidCodecParam.getEncFmt().setWidth(width);
            vidCodecParam.getEncFmt().setHeight(height);
            mEndpoint.setVideoCodecParam("H264", vidCodecParam);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public PjCall createCall(@NonNull String userIdUrl, @StreamDirectionType int callDirection,
                             @NonNull final OnCallStateListener onCallStateListener){
        if (mAccount == null) return null;
        try {
            if (!mAccount.getInfo().getRegIsActive()) return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        PjCall pjCall = new PjCall(mEndpoint, mAccount, -1,
                callDirection == StreamDirectionType.BOTH || callDirection == StreamDirectionType.STREAM_ONLY,
                callDirection == StreamDirectionType.BOTH || callDirection == StreamDirectionType.WATCH_ONLY);
        pjCall.setCallStateListener(onCallStateListener);
        CallOpParam prm = new CallOpParam(true);
        try {
            pjCall.makeCall(userIdUrl, prm);
        } catch (Exception e) {
            e.printStackTrace();
            pjCall.delete();
            pjCall = null;
        }
        return pjCall;
    }

    public void deinit() {
        /* Try force GC to avoid late destroy of PJ objects as they should be
        * deleted before lib is destroyed.
        */
        Runtime.getRuntime().gc();

        /* Shutdown pjsua. Note that Endpoint destructor will also invoke
        * libDestroy(), so this will be a test of double libDestroy().
        */
        try {
            mEndpoint.libDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* Force delete Endpoint here, to avoid deletion from a non-
        * registered thread (by GC?).
        */
        mEndpoint.delete();
        mEndpoint = null;
        Runtime.getRuntime().gc();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
