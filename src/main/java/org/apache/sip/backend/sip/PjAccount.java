package org.apache.sip.backend.sip;


import org.apache.ThreadDirector;
import org.pjsip.pjsua2.Account;
import org.pjsip.pjsua2.AccountConfig;
import org.pjsip.pjsua2.Call;
import org.pjsip.pjsua2.CallOpParam;
import org.pjsip.pjsua2.OnIncomingCallParam;
import org.pjsip.pjsua2.OnIncomingSubscribeParam;
import org.pjsip.pjsua2.OnInstantMessageParam;
import org.pjsip.pjsua2.OnRegStateParam;
import org.pjsip.pjsua2.pjsip_status_code;

/**
 * Created by Sergey Mitrofanov (GOretZ.M@gmail.com) on 02.11.15.
 */
public class PjAccount extends Account {

//    1xx: Provisional -- request received, continuing to process the request;
    private static final int PJSIP_SC_TRYING = 100;
	private static final int PJSIP_SC_RINGING = 180;
	private static final int PJSIP_SC_CALL_BEING_FORWARDED = 181;
	private static final int PJSIP_SC_QUEUED = 182;
    private static final int PJSIP_SC_PROGRESS = 183;
//    2xx: Success -- the action was successfully received, understood, and accepted;
	private static final int PJSIP_SC_OK = 200;
	private static final int PJSIP_SC_ACCEPTED = 202;
//    3xx: Redirection -- further action needs to be taken in order to complete the request;
	private static final int PJSIP_SC_MULTIPLE_CHOICES = 300;
    private static final int PJSIP_SC_MOVED_PERMANENTLY = 301;
	private static final int PJSIP_SC_MOVED_TEMPORARILY = 302;
	private static final int PJSIP_SC_USE_PROXY = 305;
	private static final int PJSIP_SC_ALTERNATIVE_SERVICE = 380;
//    4xx: Client Error -- the request contains bad syntax or cannot be fulfilled at this server;
    private static final int PJSIP_SC_BAD_REQUEST = 400;
	private static final int PJSIP_SC_UNAUTHORIZED = 401;
	private static final int PJSIP_SC_PAYMENT_REQUIRED = 402;
	private static final int PJSIP_SC_FORBIDDEN = 403;
    private static final int PJSIP_SC_NOT_FOUND = 404;
	private static final int PJSIP_SC_METHOD_NOT_ALLOWED = 405;
	private static final int PJSIP_SC_NOT_ACCEPTABLE = 406;
	private static final int PJSIP_SC_PROXY_AUTHENTICATION_REQUIRED = 407;
    private static final int PJSIP_SC_REQUEST_TIMEOUT = 408;
	private static final int PJSIP_SC_GONE = 410;
	private static final int PJSIP_SC_REQUEST_ENTITY_TOO_LARGE = 413;
	private static final int PJSIP_SC_REQUEST_URI_TOO_LONG = 414;
    private static final int PJSIP_SC_UNSUPPORTED_MEDIA_TYPE = 415;
	private static final int PJSIP_SC_UNSUPPORTED_URI_SCHEME = 416;
	private static final int PJSIP_SC_BAD_EXTENSION = 420;
	private static final int PJSIP_SC_EXTENSION_REQUIRED = 421;
    private static final int PJSIP_SC_SESSION_TIMER_TOO_SMALL = 422;
	private static final int PJSIP_SC_INTERVAL_TOO_BRIEF = 423;
	private static final int PJSIP_SC_TEMPORARILY_UNAVAILABLE = 480;
	private static final int PJSIP_SC_CALL_TSX_DOES_NOT_EXIST = 481;
    private static final int PJSIP_SC_LOOP_DETECTED = 482;
	private static final int PJSIP_SC_TOO_MANY_HOPS = 483;
	private static final int PJSIP_SC_ADDRESS_INCOMPLETE = 484;
	private static final int PJSIP_AC_AMBIGUOUS = 485;
    private static final int PJSIP_SC_BUSY_HERE = 486;
	private static final int PJSIP_SC_REQUEST_TERMINATED = 487;
	private static final int PJSIP_SC_NOT_ACCEPTABLE_HERE = 488;
	private static final int PJSIP_SC_BAD_EVENT = 489;
    private static final int PJSIP_SC_REQUEST_UPDATED = 490;
	private static final int PJSIP_SC_REQUEST_PENDING = 491;
	private static final int PJSIP_SC_UNDECIPHERABLE = 493;
//    5xx: Server Error -- the server failed to fulfill an apparently valid request;
	private static final int PJSIP_SC_INTERNAL_SERVER_ERROR = 500;
    private static final int PJSIP_SC_NOT_IMPLEMENTED = 501;
	private static final int PJSIP_SC_BAD_GATEWAY = 502;
	private static final int PJSIP_SC_SERVICE_UNAVAILABLE = 503;
	private static final int PJSIP_SC_SERVER_TIMEOUT = 504;
    private static final int PJSIP_SC_VERSION_NOT_SUPPORTED = 505;
	private static final int PJSIP_SC_MESSAGE_TOO_LARGE = 513;
	private static final int PJSIP_SC_PRECONDITION_FAILURE = 580;
//    6xx: Global Failure -- the request cannot be fulfilled at any server.
	private static final int PJSIP_SC_BUSY_EVERYWHERE = 600;
    private static final int PJSIP_SC_DECLINE = 603;
	private static final int PJSIP_SC_DOES_NOT_EXIST_ANYWHERE = 604;
	private static final int PJSIP_SC_NOT_ACCEPTABLE_ANYWHERE = 606;
	private static final int PJSIP_SC_TSX_TIMEOUT = PJSIP_SC_REQUEST_TIMEOUT;
    private static final int PJSIP_SC_TSX_TRANSPORT_ERROR = PJSIP_SC_SERVICE_UNAVAILABLE;
	private static final int PJSIP_SC__force_32bit = 0x7FFFFFFF;

    public AccountConfig cfg;

    PjAccount(AccountConfig config) {
        super();
        cfg = config;
    }

    private OnAuthListener onAuthListener;
    void create(AccountConfig cfg, OnAuthListener onAuthListener) throws Exception {
        this.onAuthListener = onAuthListener;
        super.create(cfg);
    }

    public void modify(AccountConfig cfg, OnAuthListener onAuthListener) throws Exception {
        this.onAuthListener = onAuthListener;
        super.modify(cfg);
    }

    @Override
    public void onRegState(OnRegStateParam prm) {

        final int stateCode = prm.getCode().swigValue();
        final int expiration = prm.getExpiration();
        final String reason = prm.getReason();

        ThreadDirector.INSTANCE.postToWorker(new Runnable() {
            @Override
            public void run() {
                String msg_str = "";
                if (expiration == 0) {
                    msg_str += "Unregistration";
                } else {
                    msg_str += "Registration";
                }

                if (stateCode / 100 == 2) {
                    msg_str += " successful";
//                    Log.d("Pjsip", "======== " + msg_str + " ======== ");
                    if (onAuthListener != null) onAuthListener.onSuccess();
                } else if (stateCode / 100 >= 5) {
                    msg_str += " failed: " + reason;
//                    Log.d("Pjsip", Thread.currentThread() + " ======== " + msg_str + " ======== ");
                    if (onAuthListener != null) onAuthListener.onServerFailed(msg_str);
                } else {
                    msg_str += " failed: " + reason;
//                    Log.d("Pjsip", Thread.currentThread() + " ======== " + msg_str + " ======== ");
                    if (onAuthListener != null) onAuthListener.onClientFailed(msg_str);
                }
                onAuthListener = null;
            }
        });
    }

    @Override
    public void onIncomingCall(OnIncomingCallParam prm) {
//        Log.d("Pjsip", "======== Incoming call ======== ");
//        Log.d("Pjsip", "======== " + prm.getRdata().getWholeMsg() + " ======== ");
        Call call = new Call(this, prm.getCallId());
        /* Incoming call */
        CallOpParam opParam = new CallOpParam();
        /* Drop call */
        opParam.setStatusCode(pjsip_status_code.PJSIP_SC_DECLINE);
        try {
            call.answer(opParam);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onInstantMessage(OnInstantMessageParam prm) {
//        Log.d("Pjsip", "======== Incoming pager ======== ");
//        Log.d("Pjsip", "From     : " + prm.getFromUri());
//        Log.d("Pjsip", "To       : " + prm.getToUri());
//        Log.d("Pjsip", "Contact  : " + prm.getContactUri());
//        Log.d("Pjsip", "Mimetype : " + prm.getContentType());
//        Log.d("Pjsip", "Body     : " + prm.getMsgBody());
    }

    @Override
    public void onIncomingSubscribe(OnIncomingSubscribeParam prm) {
        super.onIncomingSubscribe(prm);
    }

    public interface OnAuthListener {
        void onSuccess();
        void onServerFailed(String reason);
        void onClientFailed(String reason);
    }
}
