package org.apache.sip.backend.sip;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 11 Февраль 2017.
 */

public enum PjCoreHolder {
    INSTANCE;

    PjCore pjCore = new PjCore();

    public PjCore getPjCore() {
        return pjCore;
    }
}
