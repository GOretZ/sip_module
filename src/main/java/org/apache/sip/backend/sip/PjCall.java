package org.apache.sip.backend.sip;

import android.graphics.SurfaceTexture;
import android.support.annotation.NonNull;

import org.apache.ThreadDirector;
import org.pjsip.CameraThread;
import org.pjsip.pjsua2.AudioMedia;
import org.pjsip.pjsua2.Call;
import org.pjsip.pjsua2.CallInfo;
import org.pjsip.pjsua2.CallMediaInfo;
import org.pjsip.pjsua2.CallMediaInfoVector;
import org.pjsip.pjsua2.CallOpParam;
import org.pjsip.pjsua2.CallVidSetStreamParam;
import org.pjsip.pjsua2.Endpoint;
import org.pjsip.pjsua2.Media;
import org.pjsip.pjsua2.OnCallMediaStateParam;
import org.pjsip.pjsua2.OnCallStateParam;
import org.pjsip.pjsua2.VideoPreview;
import org.pjsip.pjsua2.VideoWindow;
import org.pjsip.pjsua2.VideoWindowHandle;
import org.pjsip.pjsua2.pjmedia_dir;
import org.pjsip.pjsua2.pjmedia_type;
import org.pjsip.pjsua2.pjsip_inv_state;
import org.pjsip.pjsua2.pjsip_role_e;
import org.pjsip.pjsua2.pjsip_status_code;
import org.pjsip.pjsua2.pjsua2;
import org.pjsip.pjsua2.pjsua_call_media_status;
import org.pjsip.pjsua2.pjsua_call_vid_strm_op;

/**
 * Created by Sergey Mitrofanov (GOretZ.M@gmail.com) on 02.11.15.
 */
public class PjCall extends Call {
    private VideoWindow vidWin;
    private VideoPreview vidPrev;
    private Endpoint mEndpoint;
    private boolean allowIncoming;
    private boolean allowOutgoing;

    public String getUri() {
        return uri;
    }

    private String uri;

    @Override
    public void makeCall(String dst_uri, CallOpParam prm) throws Exception {
        uri = dst_uri;
        super.makeCall("sip:" + dst_uri, prm);
    }

    /*PjCall(Endpoint endpoint, PjAccount acc, int call_id) {
		this(endpoint, acc, call_id, true);
	}*/

    PjCall(Endpoint endpoint, PjAccount acc, int call_id, boolean allowOutgoing, boolean allowIncoming) {
		super(acc, call_id);
        mEndpoint = endpoint;
		vidWin = null;
        this.allowIncoming = allowIncoming;
        this.allowOutgoing = allowOutgoing;
    }

    public boolean showVideo(@NonNull Object surface){
        if (vidWin != null) {
            VideoWindowHandle vidWH = new VideoWindowHandle();
            vidWH.getHandle().setWindow(surface);
            try {
                vidWin.setWindow(vidWH);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean hideVideo(){
        if (vidWin != null) {
            VideoWindowHandle vidWH = new VideoWindowHandle();
            vidWH.getHandle().setWindow(null);
            try {
                vidWin.setWindow(vidWH);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean startPreview(@NonNull Object surface){
        if (PjCoreHolder.INSTANCE.getPjCore().getCameraThread() != null) {
            PjCoreHolder.INSTANCE.getPjCore().getCameraThread().setSurfaceTexture((SurfaceTexture) surface);
            return true;
        }
        return false;
    }

    public void sendKeyFrame () {
        if (!isActive()) return;
        try {
            CallVidSetStreamParam callVidSetStreamParam = new CallVidSetStreamParam();
            vidSetStream(pjsua_call_vid_strm_op.PJSUA_CALL_VID_STRM_SEND_KEYFRAME, callVidSetStreamParam);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean stopPreview(){
        if (PjCoreHolder.INSTANCE.getPjCore().getCameraThread() != null) {
            PjCoreHolder.INSTANCE.getPjCore().getCameraThread().setSurfaceTexture(null);
            return true;
        }
        return false;
    }

    public boolean hangUp() {
        try {
            CallOpParam prm = new CallOpParam();
            prm.setStatusCode(pjsip_status_code.PJSIP_SC_DECLINE);
            hangup(prm);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean changeCamera(int cameraId) {
        if (PjCoreHolder.INSTANCE.getPjCore().getCameraThread() != null) {
            PjCoreHolder.INSTANCE.getPjCore().getCameraThread().switchCamera(cameraId, null, new CameraThread.OperationResultListener() {
                @Override
                public void onResult(int res) {
                    if (res < 0) {
                        stopPreview();
                        hangUp();
                    }
                }
            });
            return true;
        }
        return false;
    }

    @Override
    public void onCallState(OnCallStateParam prm){
        try {
            CallInfo ci = getInfo();
            if (ci.getState().swigValue() < pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED.swigValue()) {
                if (ci.getRole() == pjsip_role_e.PJSIP_ROLE_UAS) {
//                    System.out.println("Pjsip: ********************** Incoming call..");
                } else {
//                    System.out.println("Pjsip: ********************** Call other: " + ci.getStateText() + "\t(" + uri + ")");
                }
            } else if (ci.getState() == pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED) {
//                System.out.println("Pjsip: ********************** Call Confirmed: " + ci.getStateText() + "\t(" + uri + ")");
                ThreadDirector.INSTANCE.postToWorker(new Runnable() {
                    @Override
                    public void run() {
                        if (callStateListener != null){
                            callStateListener.onConnected();
                        }
                    }
                });
            } else if (ci.getState() == pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED) {
//                System.out.println("Pjsip: ********************** Call disconnected: " + ci.getStateText() + "\t(" + uri + ")");
                ThreadDirector.INSTANCE.postToWorker(new Runnable() {
                    @Override
                    public void run() {
                        if (callStateListener != null){
                            callStateListener.onDisconnected();
                        }
                    }
                });
                this.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCallMediaState(OnCallMediaStateParam prm) {
		CallInfo ci;
		try {
			ci = getInfo();
		} catch (Exception e) {
            e.printStackTrace();
            return;
		}

		CallMediaInfoVector cmiv = ci.getMedia();

        //Log.d("Pjsip", "********** There are " + cmiv.size() + " meadia streams | v/a count = " + ci.getRemVideoCount() + "/" + ci.getRemAudioCount());
		for (int i = 0; i < cmiv.size(); i++) {
			CallMediaInfo cmi = cmiv.get(i);
            /*Log.d("Pjsip", "Remote video count = " + ci.getRemVideoCount());
            Log.d("Pjsip", "Media Type = " + cmi.getType().swigValue() + " (" + cmi.getType().toString() + ")");
            Log.d("Pjsip", "Dir = " + cmi.getDir().swigValue() + " (" + cmi.getDir().toString() + ")");
            Log.d("Pjsip", "Index = " + cmi.getIndex());
            Log.d("Pjsip", "Status = " + cmi.getStatus().swigValue() + " (" + cmi.getStatus().toString() + ")");
            Log.d("Pjsip", "Cap Dev = " + cmi.getVideoCapDev());
            Log.d("Pjsip", "Incoming window id = " + cmi.getVideoIncomingWindowId());*/
			if (cmi.getType() == pjmedia_type.PJMEDIA_TYPE_AUDIO
					&& (cmi.getStatus() == pjsua_call_media_status.PJSUA_CALL_MEDIA_ACTIVE
							|| cmi.getStatus() == pjsua_call_media_status.PJSUA_CALL_MEDIA_REMOTE_HOLD) ) {
                //Log.d("Pjsip","********** This is audio stream");
				// unfortunately, on Java too, the returned Media cannot be downcasted to AudioMedia
                Media m = getMedia(i);
                AudioMedia am = AudioMedia.typecastFromMedia(m);

                try {
                    // получаем звук только если разрешено
                    if (allowIncoming) am.startTransmit(mEndpoint.audDevManager().getPlaybackDevMedia());
                    // транслируем звук только если разрешено
                    /*if (allowOutgoing)*/ mEndpoint.audDevManager().getCaptureDevMedia().startTransmit(am);
                } catch (Exception e) {
					e.printStackTrace();
				}
			} else if (cmi.getType() == pjmedia_type.PJMEDIA_TYPE_VIDEO
					&& cmi.getStatus() == pjsua_call_media_status.PJSUA_CALL_MEDIA_ACTIVE
					&& cmi.getVideoIncomingWindowId() != pjsua2.INVALID_ID) {
                //Log.d("Pjsip","********** This is video stream");
                //Log.d("Pjsip", "********************** Video manipulation...");
                // Включаем исходящий поток только если разрешено
                //if (allowOutgoing) {
                    try {
                        CallVidSetStreamParam callVidSetStreamParam = new CallVidSetStreamParam();
                        //callVidSetStreamParam.setDir(pjmedia_dir.PJMEDIA_DIR_ENCODING);
                        //vidSetStream(pjsua_call_vid_strm_op.PJSUA_CALL_VID_STRM_CHANGE_DIR, callVidSetStreamParam);
                        vidSetStream(pjsua_call_vid_strm_op.PJSUA_CALL_VID_STRM_START_TRANSMIT, callVidSetStreamParam);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                /*} else {
                    try {
                        CallVidSetStreamParam callVidSetStreamParam = new CallVidSetStreamParam();
                        callVidSetStreamParam.setDir(pjmedia_dir.PJMEDIA_DIR_DECODING);
                        vidSetStream(pjsua_call_vid_strm_op.PJSUA_CALL_VID_STRM_CHANGE_DIR, callVidSetStreamParam);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }*/
                if (allowIncoming) vidWin = new VideoWindow(cmi.getVideoIncomingWindowId());
				if (allowOutgoing) vidPrev = new VideoPreview(cmi.getVideoCapDev());
            }
        }

        //Log.d("Pjsip", "********************** Providing media callback...");
        if (vidStreamIsRunning(-1, pjmedia_dir.PJMEDIA_DIR_ENCODING)) {
            ThreadDirector.INSTANCE.postToWorker(new Runnable() {
                @Override
                public void run() {
                    if (callStateListener != null){
                        callStateListener.onMediaReady(true);
                    }
                }
            });
        } else {
            ThreadDirector.INSTANCE.postToWorker(new Runnable() {
                @Override
                public void run() {
                    if (callStateListener != null){
                        callStateListener.onMediaReady(false);
                    }
                }
            });
        }
    }

    private OnCallStateListener callStateListener = null;
    void setCallStateListener(OnCallStateListener callStateListener) {
        this.callStateListener = callStateListener;
    }

    /* логи SDP
    @Override
    public void onCallSdpCreated(OnCallSdpCreatedParam prm) {
        // Подменяем SDP
        if (allowOutgoing && !allowIncoming) prm.getSdp().setWholeSdp(prm.getSdp().getWholeSdp().replace("sendrecv", "sendonly"));
        if (allowIncoming && !allowOutgoing) prm.getSdp().setWholeSdp(prm.getSdp().getWholeSdp().replace("sendrecv", "recvonly"));
        super.onCallSdpCreated(prm);
        System.out.println("SDP: local:\n" + prm.getSdp().getWholeSdp().replaceAll("\r", ""));
        System.out.println("SDP: remote:\n" + prm.getRemSdp().getWholeSdp().replaceAll("\r", ""));
    }

    /*@Override
    public void onCallRxOffer(OnCallRxOfferParam prm) {
        super.onCallRxOffer(prm);
        System.out.println("SDP: remote offer:\n" + prm.getOffer().getWholeSdp().replaceAll("\r", ""));
    }*/
}
