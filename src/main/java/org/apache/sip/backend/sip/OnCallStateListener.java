package org.apache.sip.backend.sip;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 09 Январь 2017.
 */

public interface OnCallStateListener {
    void onDisconnected();
    void onConnected();
    void onMediaReady(boolean ready);
}
