package org.apache.sip.backend.sip;

import org.pjsip.pjsua2.LogEntry;
import org.pjsip.pjsua2.LogWriter;

/**
 * Created by Sergey Mitrofanov (GOretZ.M@gmail.com) on 02.11.15.
 */
class PjLogWriter extends LogWriter {
    @Override
    public void write(LogEntry entry) {
//		Log.d("Pjsip", entry.getMsg());
        System.out.println(entry.getMsg());
    }
}
