package org.apache.sip.backend.www;

import android.support.annotation.Size;

import org.apache.api.Api;
import org.apache.api.ApiUserModuleContract;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 17 Января 2017.
 */
public enum ApiCalls implements ApiUserModuleContract {
    INSTANCE;

    private ApiMethods apiMethods;

    @Override
    public void initializeApi(String baseUrl, String siteKey, @Size(2) String language) {
        apiMethods = Api.makeApiCalls(ApiMethods.class, baseUrl, siteKey, language);
    }


    public ApiMethods getApiMethods() {
        if (apiMethods == null) {
            throw new IllegalArgumentException("API not initialized");
        }
        return apiMethods;
    }
}
