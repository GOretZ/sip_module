package org.apache.sip.backend.www;

import org.apache.sip.entity.SipSettings;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 17 Января 2017.
 * All API methods are here
 *
 */
public interface ApiMethods {
    @GET("GetSipSettings")
    Call<SipSettings> getSipSettings(@Query(ApiParams.USER_GENDER) int userGender);
}
