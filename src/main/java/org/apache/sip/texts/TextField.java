package org.apache.sip.texts;

import org.apache.vip.templates.texts.ATextField;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 15 Февраля 2017.
 */

public enum TextField implements ATextField{
    SERVICE_BUSY,
    PARTNER_STOPPED_STREAMING,
    YOU_STOPPED_VIEWING,
    FAILED_TO_START_VIDEO,
    CONNECTION_ERROR,
    VIDEO_CAMERA,
    ROTATE_BACK_CAM,
    ROTATE_FRONT_CAM;

    private String value;

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }
}
