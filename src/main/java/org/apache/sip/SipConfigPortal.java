package org.apache.sip;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 07 Февраль 2017.
 */

public enum SipConfigPortal {
    INSTANCE;

    // Сколько миллисекунд ждать после выключения видео до завершения звонка.
    // Это для поворота экрана, чтобы не перезванивать лишний раз
    private long watchCallHangupDelay = 1000;
    private long streamCallHangupDelay = 1000;

    public long getWatchCallHangupDelay() {
        return watchCallHangupDelay;
    }

    public void setWatchCallHangupDelay(long watchCallHangupDelay) {
        this.watchCallHangupDelay = watchCallHangupDelay;
    }

    public long getStreamCallHangupDelay() {
        return streamCallHangupDelay;
    }

    public void setStreamCallHangupDelay(long streamCallHangupDelay) {
        this.streamCallHangupDelay = streamCallHangupDelay;
    }

    private byte[] salt = {-2,   55,   -78,	   67,
                          114,	-61,	95,     2,
                         -112,	113,   -69,	  -44,
                           -1,   39,  -118,   -78};

    public byte[] getSalt() {
        return salt;
    }
}
