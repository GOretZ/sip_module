package org.apache.sip.entity;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 24 Февраля 2017.
 */

// Тип ошибки
@IntDef({
        CallState.CALL_CONNECTED,
        CallState.MEDIA_READY,
        CallState.MEDIA_FAILED,
        CallState.CALL_DISCONNECTED,
})
@Retention(RetentionPolicy.SOURCE)
public @interface CallState {
    int CALL_CONNECTED = 0;
    int MEDIA_READY = 1;
    int MEDIA_FAILED = 2;
    int CALL_DISCONNECTED = 3;

}
