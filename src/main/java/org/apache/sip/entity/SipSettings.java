package org.apache.sip.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 15 Ноябрь 2016.
 */

public class SipSettings {
    @SerializedName("sip_host")
    @Expose
    private String sipHost;
    @SerializedName("sip_port")
    @Expose
    private int sipPort;
    @SerializedName("sip_transport")
    @Expose
    private String sipTransport;
    @SerializedName("sip_password")
    @Expose
    private String sipPassword;
    @SerializedName("sip_unsubscription_timeout")
    @Expose
    private long sipUnsubscriptionTimeout;
    @SerializedName("sip_subscription_timeout")
    @Expose
    private long sipSubscriptionTimeout;
    @SerializedName("sip_cam_stat_on")
    @Expose
    private int sipCamStatOn;

    public SipSettings() {
    }

    public SipSettings(String sipHost, int sipPort, String sipTransport, String sipPassword) {
        this.sipHost = sipHost;
        this.sipPort = sipPort;
        this.sipTransport = sipTransport;
        this.sipPassword = sipPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SipSettings that = (SipSettings) o;

        if (sipPort != that.sipPort) return false;
        if (sipUnsubscriptionTimeout != that.sipUnsubscriptionTimeout) return false;
        if (sipSubscriptionTimeout != that.sipSubscriptionTimeout) return false;
        if (sipCamStatOn != that.sipCamStatOn) return false;
        if (sipHost != null ? !sipHost.equals(that.sipHost) : that.sipHost != null) return false;
        if (sipTransport != null ? !sipTransport.equals(that.sipTransport) : that.sipTransport != null)
            return false;
        return sipPassword != null ? sipPassword.equals(that.sipPassword) : that.sipPassword == null;

    }

    @Override
    public int hashCode() {
        int result = sipHost != null ? sipHost.hashCode() : 0;
        result = 31 * result + sipPort;
        result = 31 * result + (sipTransport != null ? sipTransport.hashCode() : 0);
        result = 31 * result + (sipPassword != null ? sipPassword.hashCode() : 0);
        result = 31 * result + (int) (sipUnsubscriptionTimeout ^ (sipUnsubscriptionTimeout >>> 32));
        result = 31 * result + (int) (sipSubscriptionTimeout ^ (sipSubscriptionTimeout >>> 32));
        result = 31 * result + sipCamStatOn;
        return result;
    }

    public String getSipHost() {
        return sipHost;
    }

    public void setSipHost(String sipHost) {
        this.sipHost = sipHost;
    }

    public int getSipPort() {
        return sipPort;
    }

    public void setSipPort(int sipPort) {
        this.sipPort = sipPort;
    }

    public String getSipTransport() {
        return sipTransport;
    }

    public void setSipTransport(String sipTransport) {
        this.sipTransport = sipTransport;
    }

    public String getSipPassword() {
        return sipPassword;
    }

    public void setSipPassword(String sipPassword) {
        this.sipPassword = sipPassword;
    }

    public long getSipUnsubscriptionTimeout() {
        return sipUnsubscriptionTimeout;
    }

    public void setSipUnsubscriptionTimeout(long sipUnsubscriptionTimeout) {
        this.sipUnsubscriptionTimeout = sipUnsubscriptionTimeout;
    }

    public long getSipSubscriptionTimeout() {
        return sipSubscriptionTimeout;
    }

    public void setSipSubscriptionTimeout(long sipSubscriptionTimeout) {
        this.sipSubscriptionTimeout = sipSubscriptionTimeout;
    }

    public int getSipCamStatOn() {
        return sipCamStatOn;
    }

    public void setSipCamStatOn(int sipCamStatOn) {
        this.sipCamStatOn = sipCamStatOn;
    }
}
