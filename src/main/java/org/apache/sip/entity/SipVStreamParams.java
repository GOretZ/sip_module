package org.apache.sip.entity;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 24 Февраля 2017.
 */

public class SipVStreamParams {
    private final String uri;
    private final float aspectRatio;
    private final int cameraRotation;
    private final boolean cameraMirrored;
    private final boolean cameraRotated180;

    public SipVStreamParams(String uri, float aspectRatio, int cameraRotation, boolean cameraMirrored, boolean cameraRotated180) {
        this.uri = uri;
        this.aspectRatio = aspectRatio;
        this.cameraRotation = cameraRotation;
        this.cameraMirrored = cameraMirrored;
        this.cameraRotated180 = cameraRotated180;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SipVStreamParams that = (SipVStreamParams) o;

        if (Float.compare(that.aspectRatio, aspectRatio) != 0) return false;
        if (cameraRotation != that.cameraRotation) return false;
        return uri != null ? uri.equals(that.uri) : that.uri == null;

    }

    @Override
    public int hashCode() {
        int result = uri != null ? uri.hashCode() : 0;
        result = 31 * result + (aspectRatio != +0.0f ? Float.floatToIntBits(aspectRatio) : 0);
        result = 31 * result + cameraRotation;
        return result;
    }

    public String getUri() {
        return uri;
    }

    public SipVStreamParams setUri(String uri) {
        return new SipVStreamParams(uri, aspectRatio, cameraRotation, cameraMirrored, cameraRotated180);
    }

    public float getAspectRatio() {
        return aspectRatio;
    }

    public SipVStreamParams setAspectRatio(float aspectRatio) {
        return new SipVStreamParams(uri, aspectRatio, cameraRotation, cameraMirrored, cameraRotated180);
    }

    public int getCameraRotation() {
        return cameraRotation;
    }

    public SipVStreamParams setCameraRotation(int cameraRotation) {
        return new SipVStreamParams(uri, aspectRatio, cameraRotation, cameraMirrored, cameraRotated180);
    }

    public boolean isCameraMirrored() {
        return cameraMirrored;
    }

    public SipVStreamParams setCameraMirrored(boolean cameraMirrored) {
        return new SipVStreamParams(uri, aspectRatio, cameraRotation, cameraMirrored, cameraRotated180);
    }

    public boolean isCameraRotated180() {
        return cameraRotated180;
    }

    public SipVStreamParams setCameraRotated180(boolean cameraRotated180) {
        return new SipVStreamParams(uri, aspectRatio, cameraRotation, cameraMirrored, cameraRotated180);
    }
}
