package org.apache.sip.entity.state;

import org.apache.sip.entity.CameraDevice;
import org.apache.sip.entity.SipSettings;

import java.util.ArrayList;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 17 Января 2017.
 */
public enum RuntimeCache {
    INSTANCE;

    private SipSettings sipSettings;

    public SipSettings getSipSettings() {
        return sipSettings;
    }

    public void setSipSettings(SipSettings sipSettings) {
        this.sipSettings = sipSettings;
    }

    private boolean rotateBack180;

    public boolean isRotateBack180() {
        return rotateBack180;
    }

    public void setRotateBack180(boolean rotateBack180) {
        this.rotateBack180 = rotateBack180;
    }

    private boolean rotateFront180;

    public boolean isRotateFront180() {
        return rotateFront180;
    }

    public void setRotateFront180(boolean rotateFront180) {
        this.rotateFront180 = rotateFront180;
    }
}
