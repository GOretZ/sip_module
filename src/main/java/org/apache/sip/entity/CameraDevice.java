package org.apache.sip.entity;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 17 Января 2017.
 */
public class CameraDevice {

    public static final int FACING_FRONT = 1;
    public static final int FACING_BACK = 2;

    private int id;
    private String name;
    private int facing = 0;
    private int orientation = 0;

    public CameraDevice() {
    }

    public CameraDevice(int id) {
        this.id = id;
    }

    public CameraDevice(int id, String name, int facing, int orientation) {
        this.id = id;
        this.name = name;
        this.facing = facing;
        this.orientation = orientation;
    }

    @Override
    public String toString() {
        return "CameraDevice{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", facing=" + facing +
                ", orientation=" + orientation +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CameraDevice that = (CameraDevice) o;

        if (id != that.id) return false;
        if (facing != that.facing) return false;
        if (orientation != that.orientation) return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + facing;
        result = 31 * result + orientation;
        return result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFacing() {
        return facing;
    }

    public void setFacing(int facing) {
        this.facing = facing;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }
}
