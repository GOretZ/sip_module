package org.apache.sip.entity;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 17 Января 2017.
 */

// Тип ошибки
@IntDef({
        ErrorType.CAMERA_OR_AUDIO_NOT_ALLOWED,
        ErrorType.CALL_BUSY,
        ErrorType.NOT_CONNECTED,
        ErrorType.CONNECTION_FAILED,
        ErrorType.START_VIDEO_FAILED,
        ErrorType.STOP_VIDEO_FAILED,
        ErrorType.HANGUP_FAILED,
        ErrorType.CAMERA_CHANGE_FAILED
})
@Retention(RetentionPolicy.SOURCE)
public @interface ErrorType {
    int CAMERA_OR_AUDIO_NOT_ALLOWED = 0;
    int CALL_BUSY = 1;
    int NOT_CONNECTED = 2;
    int CONNECTION_FAILED = 3;
    int START_VIDEO_FAILED = 4;
    int STOP_VIDEO_FAILED = 6;
    int HANGUP_FAILED = 7;
    int CAMERA_CHANGE_FAILED = 8;
}
