package org.apache.sip.vip_gateway_ports;

import android.Manifest;

import org.apache.platform.AppPermissionChecker;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 07 Февраль 2017.
 */

public class SipPermissionChecker extends AppPermissionChecker {
    @Override
    public String[] getPermissions() {
        return new String[] {Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA};
    }
}
