package org.apache.sip.vip_gateway_ports;

import android.support.annotation.NonNull;

import org.apache.ThreadDirector;
import org.apache.sip.SipConfigPortal;
import org.apache.sip.backend.sip.OnCallStateListener;
import org.apache.sip.backend.sip.PjAccount;
import org.apache.sip.backend.sip.PjCall;
import org.apache.sip.backend.sip.PjCore;
import org.apache.sip.backend.sip.PjCoreHolder;
import org.apache.sip.backend.sip.StreamDirectionType;
import org.apache.sip.backend.www.ApiCalls;
import org.apache.sip.backend.www.ApiMethods;
import org.apache.sip.entity.CallState;
import org.apache.sip.entity.CameraDevice;
import org.apache.sip.entity.ErrorType;
import org.apache.sip.entity.SipSettings;
import org.apache.sip.entity.SipVStreamParams;
import org.apache.sip.entity.state.RuntimeCache;
import org.apache.vip.templates.gateway.AGateway;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 07 Февраль 2017.
 */

public class StreamVideoCallPort implements AGateway, StreamVideoCallPortContract {

    private static final int WIDTH = 320;
    private static final int HEIGHT = 240;
//    private static final int ROTATION = 0;

    private PjCall pjCall;
    private PjCore pjCore;
    private ApiMethods apiMethods;
    private long callHangupDelay;
    private CameraDevice cameraDevice;
    private Call<SipSettings> mCall;

    public StreamVideoCallPort() {
        this.callHangupDelay = SipConfigPortal.INSTANCE.getStreamCallHangupDelay();
        pjCore = PjCoreHolder.INSTANCE.getPjCore();
        pjCore.setStreamVideoSize(WIDTH, HEIGHT);
        apiMethods = ApiCalls.INSTANCE.getApiMethods();
    }

    // Конструктор для тестов
    StreamVideoCallPort(PjCore pjCore, ApiMethods apiMethods) {
        this.pjCore = pjCore;
        this.apiMethods = apiMethods;
    }

    private OnResultListener<Integer, Integer> onResultListener;

    @Override
    public void setOnResultListener(@NonNull OnResultListener<Integer, Integer> onResultListener) {
        this.onResultListener = onResultListener;
    }

    @Override
    public void refreshDevices() {
        pjCore.refreshDevices();
    }

    @Override
    public List<CameraDevice> getCameraDevices() {
        return pjCore.getCameraDevices(cameraDevice);
    }

    @Override
    public Integer changeCameraDevice(CameraDevice cameraDevice) {
        if (pjCall == null) return ErrorType.NOT_CONNECTED;
        if (!mediaReady) return ErrorType.CALL_BUSY;
        if (!pjCall.changeCamera(cameraDevice.getId())) return ErrorType.CAMERA_CHANGE_FAILED;
        this.cameraDevice = cameraDevice;
//        if (!pjCore.setOrientation(cameraDevice, ROTATION)) return ErrorType.CAMERA_CHANGE_FAILED;
        return null;
    }

    @Override
    public void makeCall(@NonNull final String userId, int gender) {
        if (onResultListener == null) throw new IllegalArgumentException("Где колбэк? Забыл добавить?");
//        if (cameraDevice != null) {
//            if (changeCameraDevice(cameraDevice) != null) {
//                onResultListener.onError(ErrorType.CAMERA_CHANGE_FAILED);
//                return;
//            }
//        }
        if (pjCall != null) {
            onResultListener.onError(ErrorType.CALL_BUSY);
        } else {
            mCall = apiMethods.getSipSettings(gender);
            mCall.enqueue(new Callback<SipSettings>() {
                @Override
                public void onResponse(Call<SipSettings> call, Response<SipSettings> response) {
                    mCall = null;
                    if (call.isCanceled()) return;
                    final SipSettings sipSettings = response.body();
                    if (sipSettings != null) {
                        try {
                            pjCore.authorize("u" + userId, sipSettings.getSipPassword(), sipSettings.getSipHost(),
                                    sipSettings.getSipPort(), sipSettings.getSipTransport(), new PjAccount.OnAuthListener() {
                                        @Override
                                        public void onSuccess() {
                                            if (pjCore != null) {
                                                RuntimeCache.INSTANCE.setSipSettings(sipSettings);
                                                makeCall(userId);
                                            }
                                        }

                                        @Override
                                        public void onServerFailed(String reason) {
                                            if (pjCore != null) {
//                                            System.out.println("Регистрация не прошла, проблема сервера: " + reason);
                                                makeCall(userId);
                                            }
                                        }

                                        @Override
                                        public void onClientFailed(String reason) {
                                            if (pjCore != null) {
//                                            System.out.println("Регистрация не прошла, проблема клиента: " + reason);
                                                makeCall(userId);
                                            }
                                        }
                            });
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                            makeCall(userId);
                        }
                    }
                }

                @Override
                public void onFailure(Call<SipSettings> call, Throwable t) {
                    mCall = null;
                    if (call.isCanceled()) return;
//                    System.out.println("Новые данные SIP не получены");
                    makeCall(userId);
                }
            });
        }
    }

    private boolean mediaReady;
    private void makeCall(@NonNull String userId) {
        String destination = String.format(Locale.US, "%s@%s:%s", userId,
                RuntimeCache.INSTANCE.getSipSettings().getSipHost(),
                RuntimeCache.INSTANCE.getSipSettings().getSipPort()
        );
        pjCall = pjCore.createCall(destination, StreamDirectionType.STREAM_ONLY, new OnCallStateListener() {
            @Override
            public void onDisconnected() {
                pjCall = null;
                mediaReady = false;
                cameraDevice = null;
                if (onResultListener != null) onResultListener.onDataReturned(CallState.CALL_DISCONNECTED);
            }

            @Override
            public void onConnected() {
                if (onResultListener != null) onResultListener.onDataReturned(CallState.CALL_CONNECTED);
            }

            @Override
            public void onMediaReady(boolean ready) {
                if (ready) {
                    mediaReady = true;
                    if (onResultListener != null)
                        onResultListener.onDataReturned(CallState.MEDIA_READY);
                    startSendingKeyFrames();
                } else {
                    stopPreview();
                    hangUp();
                }
            }
        });
        if (pjCall == null) {
            if (onResultListener != null) onResultListener.onError(ErrorType.CONNECTION_FAILED);
        }
    }

    private Runnable keyFramesRunnable = new Runnable() {
        @Override
        public void run() {
            if (pjCall != null && mediaReady) {
                pjCall.sendKeyFrame();
                ThreadDirector.INSTANCE.postDelayedToWorker(this, 2500);
            }
        }
    };

    private void startSendingKeyFrames() {
        ThreadDirector.INSTANCE.postDelayedToWorker(keyFramesRunnable, 2500);
    }

    @Override
    @ErrorType
    public Integer showPreview(@NonNull Object surface) {
        if (pjCall == null) return ErrorType.NOT_CONNECTED;
        ThreadDirector.INSTANCE.removeFromWorker(keyFramesRunnable);
        ThreadDirector.INSTANCE.removeFromWorker(hangupRunnable);
        startSendingKeyFrames();
        return pjCall.startPreview(surface) ? null : ErrorType.START_VIDEO_FAILED;
    }

    private Runnable hangupRunnable = new Runnable() {
        @Override
        public void run() {
            hangUp();
        }
    };

    @Override
    @ErrorType
    public Integer stopPreview() {
        if (pjCall == null) return ErrorType.NOT_CONNECTED;
        if (pjCall.stopPreview()) {
            ThreadDirector.INSTANCE.postDelayedToWorker(hangupRunnable, callHangupDelay);
            return null;
        }
        return ErrorType.STOP_VIDEO_FAILED;
    }

    @Override
    @ErrorType
    public Integer hangUp() {
        if (pjCall == null) return ErrorType.NOT_CONNECTED;
        if (!pjCall.hangUp()) {
            if (onResultListener != null) return ErrorType.HANGUP_FAILED;
        } else {
            ThreadDirector.INSTANCE.removeFromWorker(keyFramesRunnable);
            ThreadDirector.INSTANCE.removeFromWorker(hangupRunnable);
        }
        return null;
    }

    @Override
    public SipVStreamParams getVStreamParams() {
        if (pjCall != null && mediaReady)
            return new SipVStreamParams(pjCall.getUri(),
                    WIDTH * 1f / HEIGHT,
                    cameraDevice == null ? 0 : cameraDevice.getOrientation(),
                    cameraDevice != null && cameraDevice.getFacing() == CameraDevice.FACING_FRONT,
                    cameraDevice != null && (
                            cameraDevice.getFacing() == CameraDevice.FACING_FRONT ? RuntimeCache.INSTANCE.isRotateFront180() :
                            cameraDevice.getFacing() == CameraDevice.FACING_BACK && RuntimeCache.INSTANCE.isRotateBack180()
                    )
            );
        return null;
    }

    @Override
    public void destroy() {
        onResultListener = null;
        if (mCall != null) {
            mCall.cancel();
            mCall = null;
        }
        if (pjCall != null) {
            pjCall.stopPreview();
            pjCall.hangUp();
            pjCall = null;
        }
        pjCore = null;
    }
}
