package org.apache.sip.vip_gateway_ports;

import android.support.annotation.NonNull;

import org.apache.ThreadDirector;
import org.apache.sip.SipConfigPortal;
import org.apache.sip.backend.sip.OnCallStateListener;
import org.apache.sip.backend.sip.PjCall;
import org.apache.sip.backend.sip.PjCore;
import org.apache.sip.backend.sip.PjCoreHolder;
import org.apache.sip.backend.sip.StreamDirectionType;
import org.apache.sip.entity.CallState;
import org.apache.sip.entity.ErrorType;
import org.apache.vip.templates.gateway.AGateway;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 07 Февраль 2017.
 */

public class WatchVideoCallPort implements AGateway, WatchVideoCallPortContract {

    private PjCall pjCall;
    private PjCore pjCore;
    private long callHangupDelay;

    public WatchVideoCallPort() {
        this.callHangupDelay = SipConfigPortal.INSTANCE.getWatchCallHangupDelay();
        pjCore = PjCoreHolder.INSTANCE.getPjCore();
    }
	// Конструктор для тестов
    WatchVideoCallPort(PjCore pjCore) {
        this.pjCore = pjCore;
    }

    private OnResultListener<Integer, Void> onResultListener;

    @Override
    public void setOnResultListener(@NonNull OnResultListener<Integer, Void> onResultListener) {
        this.onResultListener = onResultListener;
    }

    @Override
    public void refreshDevices() {
        pjCore.refreshDevices();
    }

    @Override
    @ErrorType
    public Integer makeCall(@NonNull String destination) {
        if (onResultListener == null) throw new IllegalArgumentException("Где колбэк? Забыл добавить?");
        if (pjCall != null) {
            return ErrorType.CALL_BUSY;
        } else {
            pjCall = pjCore.createCall(destination, StreamDirectionType.WATCH_ONLY, new OnCallStateListener() {
                @Override
                public void onDisconnected() {
                    pjCall = null;
                    if (onResultListener != null) onResultListener.onDataReturned(CallState.CALL_DISCONNECTED);
                }

                @Override
                public void onConnected() {
                    if (onResultListener != null) onResultListener.onDataReturned(CallState.CALL_CONNECTED);
                }

                @Override
                public void onMediaReady(boolean ready) {
                    if (ready) {
                        if (onResultListener != null) onResultListener.onDataReturned(CallState.MEDIA_READY);
                    } else {
                        hangUp();
                    }
                }
            });
            if (pjCall == null) {
                return ErrorType.CONNECTION_FAILED;
            }
        }
        return null;
    }

    @Override
    @ErrorType
    public Integer showVideo(@NonNull Object surface) {
        if (pjCall == null) return ErrorType.NOT_CONNECTED;
        ThreadDirector.INSTANCE.removeFromWorker(hangupRunnable);
        return pjCall.showVideo(surface) ? null : ErrorType.START_VIDEO_FAILED;
    }

    private Runnable hangupRunnable = new Runnable() {
        @Override
        public void run() {
            hangUp();
        }
    };

    @Override
    @ErrorType
    public Integer hideVideo() {
        if (pjCall == null) return ErrorType.NOT_CONNECTED;
        if (pjCall.hideVideo()) {
            ThreadDirector.INSTANCE.postDelayedToWorker(hangupRunnable, callHangupDelay);
            return null;
        }
        return ErrorType.STOP_VIDEO_FAILED;
    }

    @Override
    @ErrorType
    public Integer hangUp() {
        if (pjCall == null) return ErrorType.NOT_CONNECTED;
        if (!pjCall.hangUp()) {
            if (onResultListener != null) return ErrorType.HANGUP_FAILED;
        } else {
            ThreadDirector.INSTANCE.removeFromWorker(hangupRunnable);
        }
        return null;
    }

    @Override
    public void destroy() {
        onResultListener = null;
        if (pjCall != null) {
            pjCall.hideVideo();
            pjCall.hangUp();
            pjCall = null;
        }
        pjCore = null;
    }
}
