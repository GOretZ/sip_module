package org.apache.sip.vip_gateway_ports;

import android.support.annotation.NonNull;

import org.apache.sip.entity.CameraDevice;
import org.apache.sip.entity.ErrorType;
import org.apache.sip.entity.SipVStreamParams;
import org.apache.vip.templates.AGatewayContract;

import java.util.List;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 07 Февраль 2017.
 */

public interface StreamVideoCallPortContract extends AGatewayContract {
    void setOnResultListener(@NonNull OnResultListener<Integer, Integer> onResultListener);
    void refreshDevices();
    void makeCall(@NonNull String userId, int gender);
    @ErrorType Integer showPreview(@NonNull Object surface);
    @ErrorType Integer stopPreview();
    @ErrorType Integer hangUp();
    SipVStreamParams getVStreamParams();
    List<CameraDevice> getCameraDevices();
    @ErrorType Integer changeCameraDevice(CameraDevice cameraDevice);
}
