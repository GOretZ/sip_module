package org.apache.sip.vip_gateway_ports;

import android.support.annotation.NonNull;

import org.apache.sip.entity.ErrorType;
import org.apache.vip.templates.AGatewayContract;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 07 Февраль 2017.
 */

public interface WatchVideoCallPortContract extends AGatewayContract {
    void setOnResultListener(@NonNull OnResultListener<Integer, Void> onResultListener);
    void refreshDevices();
    @ErrorType Integer makeCall(@NonNull String destination);
    @ErrorType Integer showVideo(@NonNull Object surface);
    @ErrorType Integer hideVideo();
    @ErrorType Integer hangUp();
}
