package org.apache.sip.vip_gateway_ports;

import org.apache.ThreadDirector;
import org.apache.sip.backend.sip.OnCallStateListener;
import org.apache.sip.backend.sip.PjCall;
import org.apache.sip.backend.sip.PjCore;
import org.apache.sip.backend.sip.StreamDirectionType;
import org.apache.sip.entity.CallState;
import org.apache.sip.entity.ErrorType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 20 Февраль 2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class WatchVideoCallPortTest {

    private WatchVideoCallPort watchVideoCallPort;

    @Mock
    private WatchVideoCallPortContract.OnResultListener<Integer, Void> onResultListenerMock;

    @Mock
    private PjCore pjCoreMock;

    @Mock
    private PjCall pjCallMock;

    @Mock
    private Object surfaceMock;

    @Captor
    private ArgumentCaptor<OnCallStateListener> onCallStateListenerCaptor;

    @Before
    public void setUp() throws Exception {
        ThreadDirector.INSTANCE.initForTest(false);
        MockitoAnnotations.initMocks(this);
        watchVideoCallPort = new WatchVideoCallPort(pjCoreMock);
        watchVideoCallPort.setOnResultListener(onResultListenerMock);
        when(pjCoreMock.createCall(anyString(), anyInt(), Matchers.<OnCallStateListener>any())).thenReturn(pjCallMock);
        when(pjCallMock.showVideo(surfaceMock)).thenReturn(true);
        when(pjCallMock.hideVideo()).thenReturn(true);
        when(pjCallMock.hangUp()).thenReturn(true);
    }

    @After
    public void tearDown() throws Exception {
    }

    //Должен ответить ошибкой на создание звонка
    @Test
    public void shouldAnswerErrorForCallCreation() throws Exception {
        when(pjCoreMock.createCall(anyString(), anyInt(), Matchers.<OnCallStateListener>any())).thenReturn(null);
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock);
        Integer result = watchVideoCallPort.makeCall("1234567");
        inOrder.verify(pjCoreMock).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), Matchers.<OnCallStateListener>any());
        assertEquals(result, Integer.valueOf(ErrorType.CONNECTION_FAILED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock);
    }

    //Должен ответить успехом на создание звонка, перебрав методы колбэка, включая разрыв соединения
    @Test
    public void shouldAnswerSuccessForCallCreation() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock);
        Integer result = watchVideoCallPort.makeCall("1234567");
        inOrder.verify(pjCoreMock).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        onCallStateListenerCaptor.getValue().onDisconnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_DISCONNECTED);
        assertNull(result);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock);
    }

    //Должен ответить ошибкой на создание второго звонка
    @Test
    public void shouldAnswerErrorForCallCreationOnExistent() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock);
        Integer result = watchVideoCallPort.makeCall("1234567");
        inOrder.verify(pjCoreMock).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        assertNull(result);
        result = watchVideoCallPort.makeCall("931822");
        assertEquals(result, Integer.valueOf(ErrorType.CALL_BUSY));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock);
    }

    //Должен ответить ошибкой при показе видео если не создан звонок в SIP
    @Test
    public void shouldShowVideoShowErrorNoCallCreated() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock);
        Integer result = watchVideoCallPort.showVideo(surfaceMock);
        assertEquals(result, Integer.valueOf(ErrorType.NOT_CONNECTED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock);
    }

    //Должен ответить ошибкой при ошибке показа видео после создания звонка в SIP
    @Test
    public void shouldAnswerShowVideoErrorIfCallCreated() throws Exception {
        when(pjCallMock.showVideo(surfaceMock)).thenReturn(false);
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        watchVideoCallPort.makeCall("1234567");
        inOrder.verify(pjCoreMock).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = watchVideoCallPort.showVideo(surfaceMock);
        inOrder.verify(pjCallMock).showVideo(surfaceMock);
        assertEquals(result, Integer.valueOf(ErrorType.START_VIDEO_FAILED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен ответить успехом при показе видео после создания звонка в SIP
    @Test
    public void shouldAnswerShowVideoSuccessIfCallCreated() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        watchVideoCallPort.makeCall("1234567");
        inOrder.verify(pjCoreMock).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = watchVideoCallPort.showVideo(surfaceMock);
        inOrder.verify(pjCallMock).showVideo(surfaceMock);
        assertNull(result);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен с ошибкой при попытке сменить окно для показа видео
    @Test
    public void shouldAnswerChangeVideoWindowErrorForActiveCall() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        watchVideoCallPort.makeCall("1234567");
        inOrder.verify(pjCoreMock).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        watchVideoCallPort.showVideo(surfaceMock);
        inOrder.verify(pjCallMock).showVideo(surfaceMock);
        when(pjCallMock.showVideo(surfaceMock)).thenReturn(false);
        Integer result = watchVideoCallPort.showVideo(surfaceMock);
        inOrder.verify(pjCallMock).showVideo(surfaceMock);
        assertEquals(result, Integer.valueOf(ErrorType.START_VIDEO_FAILED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен с успехом сменить окно для показа видео
    @Test
    public void shouldAnswerChangeVideoWindowSuccessForActiveCall() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        watchVideoCallPort.makeCall("1234567");
        inOrder.verify(pjCoreMock).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        watchVideoCallPort.showVideo(surfaceMock);
        inOrder.verify(pjCallMock).showVideo(surfaceMock);
        Integer result = watchVideoCallPort.showVideo(surfaceMock);
        inOrder.verify(pjCallMock).showVideo(surfaceMock);
        assertNull(result);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен ответить ошибкой при выключении видео если не создан звонок в SIP
    @Test
    public void shouldAnswerHideVideoErrorIfNoCallCreated() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock);
        Integer result = watchVideoCallPort.hideVideo();
        assertEquals(result, Integer.valueOf(ErrorType.NOT_CONNECTED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock);
    }

    //Должен ответить ошибкой при ошибке выключения видео после создания звонка в SIP
    @Test
    public void shouldAnswerHideVideoErrorIfCallCreated() throws Exception {
        when(pjCallMock.hideVideo()).thenReturn(false);
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        watchVideoCallPort.makeCall("1234567");
        inOrder.verify(pjCoreMock).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = watchVideoCallPort.hideVideo();
        inOrder.verify(pjCallMock).hideVideo();
        assertEquals(result, Integer.valueOf(ErrorType.STOP_VIDEO_FAILED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен ответить успехом при выключении видео после создания звонка в SIP
    @Test
    public void shouldAnswerHideVideoSuccessIfCallCreated() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        watchVideoCallPort.makeCall("1234567");
        inOrder.verify(pjCoreMock).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = watchVideoCallPort.hideVideo();
        inOrder.verify(pjCallMock).hideVideo();
        assertNull(result);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен ответить ошибкой на завершение звонка, если не создан звонок в SIP
    @Test
    public void shouldAnswerCallHangupFailedIfNoCallCreated() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock);
        Integer result = watchVideoCallPort.hangUp();
        assertEquals(result, Integer.valueOf(ErrorType.NOT_CONNECTED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock);
    }

    //Должен ответить ошибкой на завершение звонка
    @Test
    public void shouldAnswerErrorIfCallHangupFailed() throws Exception {
        when(pjCallMock.hangUp()).thenReturn(false);
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock);
        watchVideoCallPort.makeCall("1234567");
        inOrder.verify(pjCoreMock).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = watchVideoCallPort.hangUp();
        assertEquals(result, Integer.valueOf(ErrorType.HANGUP_FAILED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock);
    }

    //Должен ответить что звонок закончен на завершение звонка
    @Test
    public void shouldAnswerSuccessIfCallHangup() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock);
        watchVideoCallPort.makeCall("1234567");
        inOrder.verify(pjCoreMock).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = watchVideoCallPort.hangUp();
        onCallStateListenerCaptor.getValue().onDisconnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_DISCONNECTED);
        assertNull(result);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock);
    }

    /* Тесты на выжываемость звонка по таймеру */

    //Должен показать ошибку при попытке показать выключенное видео если прошло меньше секунды между вызовами
    @Test
    public void shouldAnswerShowHidedVideoError() throws Exception {
        watchVideoCallPort.makeCall("1234567");
        InOrder inOrder = inOrder(pjCallMock, onResultListenerMock);
        verify(pjCoreMock, only()).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        watchVideoCallPort.showVideo(surfaceMock);
        inOrder.verify(pjCallMock).showVideo(surfaceMock);
        watchVideoCallPort.hideVideo();
        inOrder.verify(pjCallMock).hideVideo();
        when(pjCallMock.showVideo(surfaceMock)).thenReturn(false);
        Integer result = watchVideoCallPort.showVideo(surfaceMock);
        inOrder.verify(pjCallMock).showVideo(surfaceMock);
        assertEquals(result, Integer.valueOf(ErrorType.START_VIDEO_FAILED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, onResultListenerMock);
    }

    //Должен показать выключенное видео если прошло меньше секунды между вызовами
    @Test
    public void shouldAnswerShowHidedVideoSuccess() throws Exception {
        watchVideoCallPort.makeCall("1234567");
        InOrder inOrder = inOrder(pjCallMock, onResultListenerMock);
        verify(pjCoreMock, only()).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        watchVideoCallPort.showVideo(surfaceMock);
        inOrder.verify(pjCallMock).showVideo(surfaceMock);
        watchVideoCallPort.hideVideo();
        inOrder.verify(pjCallMock).hideVideo();
        Integer result = watchVideoCallPort.showVideo(surfaceMock);
        inOrder.verify(pjCallMock).showVideo(surfaceMock);
        assertNull(result);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, onResultListenerMock);
    }

    //Должен завершить звонок, если прошло больше секунды между вызовами
    @Test
    public void shouldHangUpCallIfTimeIsOut() throws Exception {
        watchVideoCallPort.makeCall("1234567");
        InOrder inOrder = inOrder(pjCallMock, onResultListenerMock);
        verify(pjCoreMock, only()).createCall(eq("1234567"), eq(StreamDirectionType.WATCH_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        watchVideoCallPort.showVideo(surfaceMock);
        inOrder.verify(pjCallMock).showVideo(surfaceMock);
        watchVideoCallPort.hideVideo();
        inOrder.verify(pjCallMock).hideVideo();
        inOrder.verify(pjCallMock).hangUp();
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, onResultListenerMock);
    }

    //Должен обновить список девайсов
    @Test
    public void shouldRefreshDevices() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, pjCoreMock, onResultListenerMock);
        watchVideoCallPort.refreshDevices();
        inOrder.verify(pjCoreMock).refreshDevices();
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, pjCoreMock, onResultListenerMock);
    }
}