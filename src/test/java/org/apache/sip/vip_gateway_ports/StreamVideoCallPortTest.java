package org.apache.sip.vip_gateway_ports;

import org.apache.ThreadDirector;
import org.apache.sip.backend.sip.OnCallStateListener;
import org.apache.sip.backend.sip.PjAccount;
import org.apache.sip.backend.sip.PjCall;
import org.apache.sip.backend.sip.PjCore;
import org.apache.sip.backend.sip.StreamDirectionType;
import org.apache.sip.backend.www.ApiMethods;
import org.apache.sip.entity.CallState;
import org.apache.sip.entity.CameraDevice;
import org.apache.sip.entity.ErrorType;
import org.apache.sip.entity.SipSettings;
import org.apache.sip.entity.SipVStreamParams;
import org.apache.sip.entity.state.RuntimeCache;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 25 Февраль 2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class StreamVideoCallPortTest {

    private StreamVideoCallPort streamVideoCallPort;

    @Mock
    private WatchVideoCallPortContract.OnResultListener<Integer, Integer> onResultListenerMock;

    @Mock
    private ApiMethods apiMethodsMock;

    @Mock
    private Call<SipSettings> getSipSettingsMock;

    @Mock
    private PjCore pjCoreMock;

    @Mock
    private PjCall pjCallMock;

    @Mock
    private Object surfaceMock;

    @Captor
    private ArgumentCaptor<Callback<SipSettings>> apiCallbackCaptor;

    @Captor
    private ArgumentCaptor<OnCallStateListener> onCallStateListenerCaptor;

    @Captor
    private ArgumentCaptor<PjAccount.OnAuthListener> onAuthListenerCaptor;

    @Before
    public void setUp() throws Exception {
        ThreadDirector.INSTANCE.initForTest(false);
        MockitoAnnotations.initMocks(this);
        streamVideoCallPort = new StreamVideoCallPort(pjCoreMock, apiMethodsMock);
        streamVideoCallPort.setOnResultListener(onResultListenerMock);
        RuntimeCache.INSTANCE.setSipSettings(new SipSettings("host", 1010, "tcp", "pass"));
        when(pjCoreMock.createCall(anyString(), anyInt(), Matchers.<OnCallStateListener>any())).thenReturn(pjCallMock);
        when(pjCallMock.startPreview(surfaceMock)).thenReturn(true);
        when(pjCallMock.stopPreview()).thenReturn(true);
        when(pjCallMock.hangUp()).thenReturn(true);
        when(pjCallMock.changeCamera(anyInt())).thenReturn(true);
        when(apiMethodsMock.getSipSettings(anyInt())).thenReturn(getSipSettingsMock);
    }

    @After
    public void tearDown() throws Exception {
    }

    //Должен получить настройки сип с сервера, переавторизироваться и сохранить их, и использовать для создания звонка
    @Test
    public void shouldGetSipSettingsFromServerAuthorizeAndCall() throws Exception {
        when(pjCoreMock.createCall(anyString(), anyInt(), Matchers.<OnCallStateListener>any())).thenReturn(null);
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock, apiMethodsMock, getSipSettingsMock);
        streamVideoCallPort.makeCall("1234567", 1);
        inOrder.verify(apiMethodsMock).getSipSettings(1);
        inOrder.verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        Response<SipSettings> response = Response.success(new SipSettings("h", 10, "udp", "p"));
        apiCallbackCaptor.getValue().onResponse(null, response);
        inOrder.verify(pjCoreMock).authorize(eq("u1234567"), eq("p"), eq("h"), eq(10), eq("udp"), onAuthListenerCaptor.capture());
        onAuthListenerCaptor.getValue().onSuccess();
        assertEquals(RuntimeCache.INSTANCE.getSipSettings(), new SipSettings("h", 10, "udp", "p"));
        inOrder.verify(pjCoreMock).createCall(eq("1234567@h:10"), eq(StreamDirectionType.STREAM_ONLY), Matchers.<OnCallStateListener>any());
        inOrder.verify(onResultListenerMock).onError(ErrorType.CONNECTION_FAILED);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock, apiMethodsMock, getSipSettingsMock);
    }

    //Должен получить настройки сип с сервера, провально не переавторизироваться и не сохранить их, и использовать старые для создания звонка
    @Test
    public void shouldGetSipSettingsFromServerFailToAuthorizeAndCall() throws Exception {
        when(pjCoreMock.createCall(anyString(), anyInt(), Matchers.<OnCallStateListener>any())).thenReturn(null);
        doThrow(new IllegalArgumentException("ae")).when(pjCoreMock).authorize(anyString(), anyString(), anyString(), anyInt(), anyString(), onAuthListenerCaptor.capture());
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock, apiMethodsMock, getSipSettingsMock);
        streamVideoCallPort.makeCall("1234567", 1);
        inOrder.verify(apiMethodsMock).getSipSettings(1);
        inOrder.verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onResponse(null, Response.success(new SipSettings("h", 10, "udp", "p")));
        inOrder.verify(pjCoreMock).authorize(eq("u1234567"), eq("p"), eq("h"), eq(10), eq("udp"), onAuthListenerCaptor.capture());
        assertEquals(RuntimeCache.INSTANCE.getSipSettings(), new SipSettings("host", 1010, "tcp", "pass"));
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), Matchers.<OnCallStateListener>any());
        inOrder.verify(onResultListenerMock).onError(ErrorType.CONNECTION_FAILED);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock, apiMethodsMock, getSipSettingsMock);
    }

    //Должен получить настройки сип с сервера, не суметь переавторизироваться и не сохранить их, и использовать старые для создания звонка
    @Test
    public void shouldGetSipSettingsFromServerNotAuthorizeAndCall() throws Exception {
        when(pjCoreMock.createCall(anyString(), anyInt(), Matchers.<OnCallStateListener>any())).thenReturn(null);
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock, apiMethodsMock, getSipSettingsMock);
        streamVideoCallPort.makeCall("1234567", 1);
        inOrder.verify(apiMethodsMock).getSipSettings(1);
        inOrder.verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onResponse(null, Response.success(new SipSettings("h", 10, "udp", "p")));
        inOrder.verify(pjCoreMock).authorize(eq("u1234567"), eq("p"), eq("h"), eq(10), eq("udp"), onAuthListenerCaptor.capture());
        onAuthListenerCaptor.getValue().onClientFailed("dsada");
        assertEquals(RuntimeCache.INSTANCE.getSipSettings(), new SipSettings("host", 1010, "tcp", "pass"));
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), Matchers.<OnCallStateListener>any());
        inOrder.verify(onResultListenerMock).onError(ErrorType.CONNECTION_FAILED);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock, apiMethodsMock, getSipSettingsMock);
    }

    //Должен не суметь получить настройки сип с сервера, и использовать старые для создания звонка
    @Test
    public void shouldNotGetSipSettingsFromServerAndCall() throws Exception {
        when(pjCoreMock.createCall(anyString(), anyInt(), Matchers.<OnCallStateListener>any())).thenReturn(null);
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock, apiMethodsMock, getSipSettingsMock);
        streamVideoCallPort.makeCall("1234567", 1);
        inOrder.verify(apiMethodsMock).getSipSettings(1);
        inOrder.verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        assertEquals(RuntimeCache.INSTANCE.getSipSettings(), new SipSettings("host", 1010, "tcp", "pass"));
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), Matchers.<OnCallStateListener>any());
        inOrder.verify(onResultListenerMock).onError(ErrorType.CONNECTION_FAILED);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock, apiMethodsMock, getSipSettingsMock);
    }

    //Должен ответить ошибкой на создание звонка
    @Test
    public void shouldAnswerErrorForCallCreation() throws Exception {
        when(pjCoreMock.createCall(anyString(), anyInt(), Matchers.<OnCallStateListener>any())).thenReturn(null);
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), Matchers.<OnCallStateListener>any());
        inOrder.verify(onResultListenerMock).onError(ErrorType.CONNECTION_FAILED);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock);
    }

    //Должен ответить успехом на создание звонка, перебрав методы колбэка, включая разрыв соединения
    @Test
    public void shouldAnswerSuccessForCallCreation() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        onCallStateListenerCaptor.getValue().onDisconnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_DISCONNECTED);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock);
    }

    //Должен ответить ошибкой на создание второго звонка
    @Test
    public void shouldAnswerErrorForCallCreationOnExistent() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        streamVideoCallPort.makeCall("931822", 1);
        inOrder.verify(onResultListenerMock).onError(ErrorType.CALL_BUSY);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock);
    }

    //Должен ответить ошибкой при показе видео если не создан звонок в SIP
    @Test
    public void shouldShowVideoShowErrorNoCallCreated() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock);
        Integer result = streamVideoCallPort.showPreview(surfaceMock);
        assertEquals(result, Integer.valueOf(ErrorType.NOT_CONNECTED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock);
    }

    //Должен ответить ошибкой при ошибке показа видео после создания звонка в SIP
    @Test
    public void shouldAnswerShowVideoErrorIfCallCreated() throws Exception {
        when(pjCallMock.startPreview(surfaceMock)).thenReturn(false);
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        assertEquals(result, Integer.valueOf(ErrorType.START_VIDEO_FAILED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен ответить успехом при показе видео после создания звонка в SIP
    @Test
    public void shouldAnswerShowVideoSuccessIfCallCreated() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        assertNull(result);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен ответить ошибкой при попытке сменить окно для показа видео
    @Test
    public void shouldAnswerChangeVideoWindowErrorForActiveCall() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        when(pjCallMock.startPreview(surfaceMock)).thenReturn(false);
        Integer result = streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        assertEquals(result, Integer.valueOf(ErrorType.START_VIDEO_FAILED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен с успехом сменить окно для показа видео
    @Test
    public void shouldAnswerChangeVideoWindowSuccessForActiveCall() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        assertNull(result);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен ответить ошибкой при выключении видео если не создан звонок в SIP
    @Test
    public void shouldAnswerHideVideoErrorIfNoCallCreated() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock);
        Integer result = streamVideoCallPort.stopPreview();
        assertEquals(result, Integer.valueOf(ErrorType.NOT_CONNECTED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock);
    }

    //Должен ответить ошибкой при ошибке выключения видео после создания звонка в SIP
    @Test
    public void shouldAnswerHideVideoErrorIfCallCreated() throws Exception {
        when(pjCallMock.stopPreview()).thenReturn(false);
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = streamVideoCallPort.stopPreview();
        inOrder.verify(pjCallMock).stopPreview();
        assertEquals(result, Integer.valueOf(ErrorType.STOP_VIDEO_FAILED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен ответить успехом при выключении видео после создания звонка в SIP
    @Test
    public void shouldAnswerHideVideoSuccessIfCallCreated() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock, pjCallMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = streamVideoCallPort.stopPreview();
        inOrder.verify(pjCallMock).stopPreview();
        assertNull(result);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock, pjCallMock);
    }

    //Должен ответить ошибкой на завершение звонка, если не создан звонок в SIP
    @Test
    public void shouldAnswerCallHangupFailedIfNoCallCreated() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock);
        Integer result = streamVideoCallPort.hangUp();
        assertEquals(result, Integer.valueOf(ErrorType.NOT_CONNECTED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock);
    }

    //Должен ответить ошибкой на завершение звонка
    @Test
    public void shouldAnswerErrorIfCallHangupFailed() throws Exception {
        when(pjCallMock.hangUp()).thenReturn(false);
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = streamVideoCallPort.hangUp();
        assertEquals(result, Integer.valueOf(ErrorType.HANGUP_FAILED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock);
    }

    //Должен ответить что звонок закончен на завершение звонка
    @Test
    public void shouldAnswerSuccessIfCallHangup() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = streamVideoCallPort.hangUp();
        onCallStateListenerCaptor.getValue().onDisconnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_DISCONNECTED);
        assertNull(result);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock);
    }

    /* Тесты на выжываемость звонка по таймеру */

    //Должен показать ошибку при попытке показать выключенное видео если прошло меньше секунды между вызовами
    @Test
    public void shouldAnswerShowHidedVideoError() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        verify(pjCoreMock, only()).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        streamVideoCallPort.stopPreview();
        inOrder.verify(pjCallMock).stopPreview();
        when(pjCallMock.startPreview(surfaceMock)).thenReturn(false);
        Integer result = streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        assertEquals(result, Integer.valueOf(ErrorType.START_VIDEO_FAILED));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, onResultListenerMock);
    }

    //Должен показать выключенное видео если прошло меньше секунды между вызовами
    @Test
    public void shouldAnswerShowHidedVideoSuccess() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        verify(pjCoreMock, only()).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        streamVideoCallPort.stopPreview();
        inOrder.verify(pjCallMock).stopPreview();
        Integer result = streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        assertNull(result);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, onResultListenerMock);
    }

    //Должен завершить звонок, если прошло больше секунды между вызовами
    @Test
    public void shouldHangUpCallIfTimeIsOut() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        verify(pjCoreMock, only()).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        streamVideoCallPort.stopPreview();
        inOrder.verify(pjCallMock).stopPreview();
        inOrder.verify(pjCallMock).hangUp();
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, onResultListenerMock);
    }

    //Должен завершить звонок сразу
    @Test
    public void shouldHangUpCallWithPreviewStop() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        verify(pjCoreMock, only()).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        streamVideoCallPort.stopPreview();
        inOrder.verify(pjCallMock).stopPreview();
        streamVideoCallPort.hangUp();
        inOrder.verify(pjCallMock).hangUp();
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, onResultListenerMock);
    }

    //Должен завершить звонок сразу
    @Test
    public void shouldHangUpCall() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        verify(pjCoreMock, only()).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        inOrder.verify(pjCallMock).sendKeyFrame();
        streamVideoCallPort.showPreview(surfaceMock);
        inOrder.verify(pjCallMock).startPreview(surfaceMock);
        streamVideoCallPort.hangUp();
        inOrder.verify(pjCallMock).hangUp();
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, onResultListenerMock);
    }

    //Должен сменить ориентацию
//    @Test
//    public void shouldChangeOrientation() throws Exception {
//        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock, delayedExecutorMock);
//        streamVideoCallPort.setOrientation(0);
//        inOrder.verify(pjCoreMock).setOrientation(0, 240, 320);
//        streamVideoCallPort.setOrientation(90);
//        inOrder.verify(pjCoreMock).setOrientation(90, 320, 240);
//        streamVideoCallPort.setOrientation(180);
//        inOrder.verify(pjCoreMock).setOrientation(180, 240, 320);
//        streamVideoCallPort.setOrientation(270);
//        inOrder.verify(pjCoreMock).setOrientation(270, 320, 240);
//        inOrder.verifyNoMoreInteractions();
//    }

    //Должен вернуть параметры стрима - ничего
    @Test
    public void shouldReturnNullForStreamParams() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, pjCallMock, onResultListenerMock);
        assertNull(streamVideoCallPort.getVStreamParams());
//        streamVideoCallPort.setOrientation(0);
//        inOrder.verify(pjCoreMock).setOrientation(0, 240, 320);
//        assertNull(streamVideoCallPort.getVStreamParams());
//        streamVideoCallPort.setOrientation(90);
//        inOrder.verify(pjCoreMock).setOrientation(90, 320, 240);
//        assertNull(streamVideoCallPort.getVStreamParams());
//        streamVideoCallPort.setOrientation(180);
//        inOrder.verify(pjCoreMock).setOrientation(180, 240, 320);
//        assertNull(streamVideoCallPort.getVStreamParams());
//        streamVideoCallPort.setOrientation(270);
//        inOrder.verify(pjCoreMock).setOrientation(270, 320, 240);
//        assertNull(streamVideoCallPort.getVStreamParams());
        verifyNoMoreInteractions(pjCoreMock, pjCallMock, onResultListenerMock);
    }

    //Должен вернуть параметры стрима - ничего
    @Test
    public void shouldReturnNullForStreamParamsWhileMediaNotReady() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        assertNull(streamVideoCallPort.getVStreamParams());
//        streamVideoCallPort.setOrientation(0);
//        inOrder.verify(pjCoreMock).setOrientation(0, 240, 320);
//        assertNull(streamVideoCallPort.getVStreamParams());
//        streamVideoCallPort.setOrientation(90);
//        inOrder.verify(pjCoreMock).setOrientation(90, 320, 240);
//        assertNull(streamVideoCallPort.getVStreamParams());
//        streamVideoCallPort.setOrientation(180);
//        inOrder.verify(pjCoreMock).setOrientation(180, 240, 320);
//        assertNull(streamVideoCallPort.getVStreamParams());
//        streamVideoCallPort.setOrientation(270);
//        inOrder.verify(pjCoreMock).setOrientation(270, 320, 240);
//        assertNull(streamVideoCallPort.getVStreamParams());
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock);
    }

    //Должен обновить список девайсов
    @Test
    public void shouldRefreshDevices() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, pjCoreMock, onResultListenerMock);
        streamVideoCallPort.refreshDevices();
        inOrder.verify(pjCoreMock).refreshDevices();
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, pjCoreMock, onResultListenerMock);
    }

    //Должен выдать список камер
    @Test
    public void shouldReturnCameraDevices() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, pjCoreMock, onResultListenerMock);
        ArrayList<CameraDevice> cameraDevices = new ArrayList<>();
        cameraDevices.add(new CameraDevice(0));
        cameraDevices.add(new CameraDevice(1));
        cameraDevices.add(new CameraDevice(2));
        cameraDevices.add(new CameraDevice(3));
        when(pjCoreMock.getCameraDevices(nullable(CameraDevice.class))).thenReturn(cameraDevices);
        ArrayList<CameraDevice> cameraDevices1 = new ArrayList<>();
        cameraDevices1.add(new CameraDevice(0));
        cameraDevices1.add(new CameraDevice(1));
        cameraDevices1.add(new CameraDevice(2));
        cameraDevices1.add(new CameraDevice(3));
        assertEquals(streamVideoCallPort.getCameraDevices(), cameraDevices1);
        inOrder.verify(pjCoreMock).getCameraDevices(null);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, pjCoreMock, onResultListenerMock);
    }

    //Должен не запоминать выбранную камеру, в том числе и при запросе списка камер если звонка нету
    @Test
    public void shouldRememberCameraDeviceChosen() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, pjCoreMock, onResultListenerMock);
        streamVideoCallPort.getCameraDevices();
        inOrder.verify(pjCoreMock).getCameraDevices(null);
        streamVideoCallPort.changeCameraDevice(new CameraDevice(0));
        streamVideoCallPort.getCameraDevices();
        inOrder.verify(pjCoreMock).getCameraDevices(null);
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, pjCoreMock, onResultListenerMock);
    }

    //Должен Сообщить о неготовности смены камеры до готовности медиа звонка
    @Test
    public void shouldNotChangeCameraDeviceForCall() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, pjCoreMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        assertEquals((Integer) ErrorType.NOT_CONNECTED, streamVideoCallPort.changeCameraDevice(new CameraDevice(0)));
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        assertEquals((Integer) ErrorType.CALL_BUSY, streamVideoCallPort.changeCameraDevice(new CameraDevice(0)));
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        assertEquals((Integer) ErrorType.CALL_BUSY, streamVideoCallPort.changeCameraDevice(new CameraDevice(0)));
        onCallStateListenerCaptor.getValue().onDisconnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_DISCONNECTED);
        assertEquals((Integer) ErrorType.NOT_CONNECTED, streamVideoCallPort.changeCameraDevice(new CameraDevice(0)));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, pjCoreMock, onResultListenerMock);
    }

    //Должен Сменить камеру по готовности медиа звонка
    @Test
    public void shouldChangeCameraDeviceForCall() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, pjCoreMock, onResultListenerMock);
        assertEquals((Integer) ErrorType.NOT_CONNECTED, streamVideoCallPort.changeCameraDevice(new CameraDevice(0)));
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        assertEquals((Integer) ErrorType.CALL_BUSY, streamVideoCallPort.changeCameraDevice(new CameraDevice(0)));
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        assertEquals(null, streamVideoCallPort.changeCameraDevice(new CameraDevice(0)));
        inOrder.verify(pjCallMock).changeCamera(0);
        onCallStateListenerCaptor.getValue().onDisconnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_DISCONNECTED);
        assertEquals((Integer) ErrorType.NOT_CONNECTED, streamVideoCallPort.changeCameraDevice(new CameraDevice(0)));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, pjCoreMock, onResultListenerMock);
    }

    //Должен выдать список камер кроме уже выбранной
    @Test
    public void shouldReturnCameraDevicesExcludingTheSelectedOne() throws Exception {
        InOrder inOrder = inOrder(pjCallMock, pjCoreMock, onResultListenerMock);
        assertEquals((Integer) ErrorType.NOT_CONNECTED, streamVideoCallPort.changeCameraDevice(new CameraDevice(0)));
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        streamVideoCallPort.getCameraDevices();
        inOrder.verify(pjCoreMock).getCameraDevices(null);
        assertEquals(null, streamVideoCallPort.changeCameraDevice(new CameraDevice(1)));
        inOrder.verify(pjCallMock).changeCamera(1);
        streamVideoCallPort.getCameraDevices();
        inOrder.verify(pjCoreMock).getCameraDevices(new CameraDevice(1));
        assertEquals(null, streamVideoCallPort.changeCameraDevice(new CameraDevice(2)));
        inOrder.verify(pjCallMock).changeCamera(2);
        streamVideoCallPort.getCameraDevices();
        inOrder.verify(pjCoreMock).getCameraDevices(new CameraDevice(2));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCallMock, pjCoreMock, onResultListenerMock);
    }

    //Должен вернуть параметры стрима - верные данные
    @Test
    public void shouldReturnDataForStreamParamsW() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        when(pjCallMock.getUri()).thenReturn("sip:1234567@host:1010");
        assertEquals(streamVideoCallPort.getVStreamParams(), new SipVStreamParams("sip:1234567@host:1010", 4f / 3, 0, false, false));
        streamVideoCallPort.changeCameraDevice(new CameraDevice(0, "name", 2, 90));
        assertEquals(streamVideoCallPort.getVStreamParams(), new SipVStreamParams("sip:1234567@host:1010", 4f / 3, 90, true, false));
//        streamVideoCallPort.setOrientation(0);
//        inOrder.verify(pjCoreMock).setOrientation(0, 240, 320);
//        assertEquals(streamVideoCallPort.getVStreamParams(), new SipVStreamParams("sip:1234567@host:1010", 3f / 4));
//        streamVideoCallPort.setOrientation(90);
//        inOrder.verify(pjCoreMock).setOrientation(90, 320, 240);
//        assertEquals(streamVideoCallPort.getVStreamParams(), new SipVStreamParams("sip:1234567@host:1010", 4f / 3));
//        streamVideoCallPort.setOrientation(180);
//        inOrder.verify(pjCoreMock).setOrientation(180, 240, 320);
//        assertEquals(streamVideoCallPort.getVStreamParams(), new SipVStreamParams("sip:1234567@host:1010", 3f / 4));
//        streamVideoCallPort.setOrientation(270);
//        inOrder.verify(pjCoreMock).setOrientation(270, 320, 240);
//        assertEquals(streamVideoCallPort.getVStreamParams(), new SipVStreamParams("sip:1234567@host:1010", 4f / 3));
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock);
    }

    //Должен вернуть параметры стрима - ничего
    @Test
    public void shouldReturnNullForStreamParamsAfterCallFinished() throws Exception {
        InOrder inOrder = inOrder(pjCoreMock, onResultListenerMock);
        streamVideoCallPort.makeCall("1234567", 1);
        verify(getSipSettingsMock).enqueue(apiCallbackCaptor.capture());
        apiCallbackCaptor.getValue().onFailure(null, null);
        inOrder.verify(pjCoreMock).createCall(eq("1234567@host:1010"), eq(StreamDirectionType.STREAM_ONLY), onCallStateListenerCaptor.capture());
        onCallStateListenerCaptor.getValue().onConnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_CONNECTED);
        onCallStateListenerCaptor.getValue().onMediaReady(true);
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.MEDIA_READY);
        Integer result = streamVideoCallPort.hangUp();
        onCallStateListenerCaptor.getValue().onDisconnected();
        inOrder.verify(onResultListenerMock).onDataReturned(CallState.CALL_DISCONNECTED);
        assertNull(streamVideoCallPort.getVStreamParams());
//        streamVideoCallPort.setOrientation(0);
//        assertNull(streamVideoCallPort.getVStreamParams());
//        inOrder.verify(pjCoreMock).setOrientation(0, 240, 320);
//        streamVideoCallPort.setOrientation(90);
//        inOrder.verify(pjCoreMock).setOrientation(90, 320, 240);
//        assertNull(streamVideoCallPort.getVStreamParams());
//        streamVideoCallPort.setOrientation(180);
//        inOrder.verify(pjCoreMock).setOrientation(180, 240, 320);
//        assertNull(streamVideoCallPort.getVStreamParams());
//        streamVideoCallPort.setOrientation(270);
//        inOrder.verify(pjCoreMock).setOrientation(270, 320, 240);
//        assertNull(streamVideoCallPort.getVStreamParams());
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(pjCoreMock, onResultListenerMock);
    }
}