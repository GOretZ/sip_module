package org.apache.sip.backend.www;

import org.apache.api.Api;
import org.apache.sip.entity.SipSettings;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.junit.Assert.*;

/**
 * Created by Sergey Mitrofanov (goretz.m@gmail.com) on 07 Январь 2017.
 */
public class ApiTest {

    private final CountDownLatch signal = new CountDownLatch(1);

    @BeforeClass
    public static void setUp(){
        Api.userId = "8640035";
        Api.userLevel = "1";
        ApiCalls.INSTANCE.initializeApi("http://149.202.248.142/test/", "MaTe639dF4", "ru");
    }

    @Test
    public void getSipSettingsUnknown() throws Exception {
        ApiCalls.INSTANCE.getApiMethods().getSipSettings(0)
                .enqueue(new Callback<SipSettings>() {
                    @Override
                    public void onResponse(Call<SipSettings> call, Response<SipSettings> response) {
                        assertNotNull(response.body().getSipHost());
                        signal.countDown();// notify the count down latch
                    }

                    @Override
                    public void onFailure(Call<SipSettings> call, Throwable t) {
                        assertNull(t);
                        signal.countDown();// notify the count down latch
                    }
                });
        signal.await(30, TimeUnit.SECONDS);// wait for callback
    }

    @Test
    public void getSipSettingsMale() throws Exception {
        ApiCalls.INSTANCE.getApiMethods().getSipSettings(1)
                .enqueue(new Callback<SipSettings>() {
                    @Override
                    public void onResponse(Call<SipSettings> call, Response<SipSettings> response) {
                        assertNotNull(response.body().getSipHost());
                        signal.countDown();// notify the count down latch
                    }

                    @Override
                    public void onFailure(Call<SipSettings> call, Throwable t) {
                        assertNull(t);
                        signal.countDown();// notify the count down latch
                    }
                });
        signal.await(30, TimeUnit.SECONDS);// wait for callback
    }

    @Test
    public void getSipSettingsFeMale() throws Exception {
        ApiCalls.INSTANCE.getApiMethods().getSipSettings(2)
                .enqueue(new Callback<SipSettings>() {
                    @Override
                    public void onResponse(Call<SipSettings> call, Response<SipSettings> response) {
                        assertNotNull(response.body().getSipHost());
                        signal.countDown();// notify the count down latch
                    }

                    @Override
                    public void onFailure(Call<SipSettings> call, Throwable t) {
                        assertNull(t);
                        signal.countDown();// notify the count down latch
                    }
                });
        signal.await(30, TimeUnit.SECONDS);// wait for callback
    }
}